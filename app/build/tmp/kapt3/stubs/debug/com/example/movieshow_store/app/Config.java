package com.example.movieshow_store.app;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0018\u0000 \u000e2\u00020\u0001:\u0001\u000eB\u0005\u00a2\u0006\u0002\u0010\u0002J(\u0010\u0003\u001a\u00020\u0004\"\b\b\u0000\u0010\u0005*\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\f\u0010\t\u001a\b\u0012\u0004\u0012\u0002H\u00050\nH\u0002J\b\u0010\u000b\u001a\u00020\fH\u0016J\b\u0010\r\u001a\u00020\fH\u0002\u00a8\u0006\u000f"}, d2 = {"Lcom/example/movieshow_store/app/Config;", "Landroid/app/Application;", "()V", "getIdByTable", "Ljava/util/concurrent/atomic/AtomicInteger;", "T", "Lio/realm/RealmObject;", "realm", "Lio/realm/Realm;", "anyclass", "Ljava/lang/Class;", "onCreate", "", "setupConfig", "Companion", "app_debug"})
public final class Config extends android.app.Application {
    public static final com.example.movieshow_store.app.Config.Companion Companion = null;
    
    @java.lang.Override()
    public void onCreate() {
    }
    
    private final <T extends io.realm.RealmObject>java.util.concurrent.atomic.AtomicInteger getIdByTable(io.realm.Realm realm, java.lang.Class<T> anyclass) {
        return null;
    }
    
    private final void setupConfig() {
    }
    
    public Config() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/example/movieshow_store/app/Config$Companion;", "", "()V", "app_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}