package com.example.movieshow_store.models;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0010\b\u0016\u0018\u00002\u00020\u0001B-\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0005\u0012\b\b\u0002\u0010\u0007\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\bR\u001a\u0010\u0004\u001a\u00020\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\t\u0010\n\"\u0004\b\u000b\u0010\fR\u001a\u0010\u0007\u001a\u00020\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\n\"\u0004\b\u000e\u0010\fR\u001e\u0010\u0002\u001a\u00020\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\u0010\"\u0004\b\u0011\u0010\u0012R\u001a\u0010\u0006\u001a\u00020\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\n\"\u0004\b\u0014\u0010\f\u00a8\u0006\u0015"}, d2 = {"Lcom/example/movieshow_store/models/Cd;", "Lio/realm/RealmObject;", "id", "", "condicion", "", "ubicacion", "estado", "(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getCondicion", "()Ljava/lang/String;", "setCondicion", "(Ljava/lang/String;)V", "getEstado", "setEstado", "getId", "()I", "setId", "(I)V", "getUbicacion", "setUbicacion", "app_debug"})
public class Cd extends io.realm.RealmObject {
    @io.realm.annotations.PrimaryKey()
    private int id;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String condicion;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String ubicacion;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String estado;
    
    public final int getId() {
        return 0;
    }
    
    public final void setId(int p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getCondicion() {
        return null;
    }
    
    public final void setCondicion(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getUbicacion() {
        return null;
    }
    
    public final void setUbicacion(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getEstado() {
        return null;
    }
    
    public final void setEstado(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    public Cd(int id, @org.jetbrains.annotations.NotNull()
    java.lang.String condicion, @org.jetbrains.annotations.NotNull()
    java.lang.String ubicacion, @org.jetbrains.annotations.NotNull()
    java.lang.String estado) {
        super();
    }
    
    public Cd() {
        super();
    }
}