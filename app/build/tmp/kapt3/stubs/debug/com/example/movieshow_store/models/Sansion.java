package com.example.movieshow_store.models;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0014\b\u0016\u0018\u00002\u00020\u0001B;\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u0012\b\b\u0002\u0010\b\u001a\u00020\u0003\u0012\b\b\u0002\u0010\t\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\nR\u001c\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u001c\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\u0010\"\u0004\b\u0011\u0010\u0012R\u001a\u0010\t\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016R\u001e\u0010\u0002\u001a\u00020\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0017\u0010\u0014\"\u0004\b\u0018\u0010\u0016R\u001a\u0010\b\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\u0014\"\u0004\b\u001a\u0010\u0016\u00a8\u0006\u001b"}, d2 = {"Lcom/example/movieshow_store/models/Sansion;", "Lio/realm/RealmObject;", "id", "", "cliente", "Lcom/example/movieshow_store/models/Cliente;", "alquiler", "Lcom/example/movieshow_store/models/Alquiler;", "tipo", "dias", "(ILcom/example/movieshow_store/models/Cliente;Lcom/example/movieshow_store/models/Alquiler;II)V", "getAlquiler", "()Lcom/example/movieshow_store/models/Alquiler;", "setAlquiler", "(Lcom/example/movieshow_store/models/Alquiler;)V", "getCliente", "()Lcom/example/movieshow_store/models/Cliente;", "setCliente", "(Lcom/example/movieshow_store/models/Cliente;)V", "getDias", "()I", "setDias", "(I)V", "getId", "setId", "getTipo", "setTipo", "app_debug"})
public class Sansion extends io.realm.RealmObject {
    @io.realm.annotations.PrimaryKey()
    private int id;
    @org.jetbrains.annotations.Nullable()
    private com.example.movieshow_store.models.Cliente cliente;
    @org.jetbrains.annotations.Nullable()
    private com.example.movieshow_store.models.Alquiler alquiler;
    private int tipo;
    private int dias;
    
    public final int getId() {
        return 0;
    }
    
    public final void setId(int p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.example.movieshow_store.models.Cliente getCliente() {
        return null;
    }
    
    public final void setCliente(@org.jetbrains.annotations.Nullable()
    com.example.movieshow_store.models.Cliente p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.example.movieshow_store.models.Alquiler getAlquiler() {
        return null;
    }
    
    public final void setAlquiler(@org.jetbrains.annotations.Nullable()
    com.example.movieshow_store.models.Alquiler p0) {
    }
    
    public final int getTipo() {
        return 0;
    }
    
    public final void setTipo(int p0) {
    }
    
    public final int getDias() {
        return 0;
    }
    
    public final void setDias(int p0) {
    }
    
    public Sansion(int id, @org.jetbrains.annotations.Nullable()
    com.example.movieshow_store.models.Cliente cliente, @org.jetbrains.annotations.Nullable()
    com.example.movieshow_store.models.Alquiler alquiler, int tipo, int dias) {
        super();
    }
    
    public Sansion() {
        super();
    }
}