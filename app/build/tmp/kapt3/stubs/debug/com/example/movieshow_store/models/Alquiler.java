package com.example.movieshow_store.models;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0007\n\u0002\b\u001c\b\u0016\u0018\u00002\u00020\u0001BO\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u0012\b\b\u0002\u0010\b\u001a\u00020\t\u0012\b\b\u0002\u0010\n\u001a\u00020\u000b\u0012\b\b\u0002\u0010\f\u001a\u00020\u0003\u0012\b\b\u0002\u0010\r\u001a\u00020\t\u00a2\u0006\u0002\u0010\u000eR\u001c\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\u0010\"\u0004\b\u0011\u0010\u0012R\u001c\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016R\u001a\u0010\f\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0017\u0010\u0018\"\u0004\b\u0019\u0010\u001aR\u001a\u0010\r\u001a\u00020\tX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001b\u0010\u001c\"\u0004\b\u001d\u0010\u001eR\u001a\u0010\b\u001a\u00020\tX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001f\u0010\u001c\"\u0004\b \u0010\u001eR\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b!\u0010\u0018\"\u0004\b\"\u0010\u001aR\u001a\u0010\n\u001a\u00020\u000bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b#\u0010$\"\u0004\b%\u0010&\u00a8\u0006\'"}, d2 = {"Lcom/example/movieshow_store/models/Alquiler;", "Lio/realm/RealmObject;", "id", "", "cliente", "Lcom/example/movieshow_store/models/Cliente;", "cd", "Lcom/example/movieshow_store/models/Cd;", "fecha_prestamo", "Ljava/util/Date;", "precio", "", "dias", "fecha_devolucion", "(ILcom/example/movieshow_store/models/Cliente;Lcom/example/movieshow_store/models/Cd;Ljava/util/Date;FILjava/util/Date;)V", "getCd", "()Lcom/example/movieshow_store/models/Cd;", "setCd", "(Lcom/example/movieshow_store/models/Cd;)V", "getCliente", "()Lcom/example/movieshow_store/models/Cliente;", "setCliente", "(Lcom/example/movieshow_store/models/Cliente;)V", "getDias", "()I", "setDias", "(I)V", "getFecha_devolucion", "()Ljava/util/Date;", "setFecha_devolucion", "(Ljava/util/Date;)V", "getFecha_prestamo", "setFecha_prestamo", "getId", "setId", "getPrecio", "()F", "setPrecio", "(F)V", "app_debug"})
public class Alquiler extends io.realm.RealmObject {
    private int id;
    @org.jetbrains.annotations.Nullable()
    private com.example.movieshow_store.models.Cliente cliente;
    @org.jetbrains.annotations.Nullable()
    private com.example.movieshow_store.models.Cd cd;
    @org.jetbrains.annotations.NotNull()
    private java.util.Date fecha_prestamo;
    private float precio;
    private int dias;
    @org.jetbrains.annotations.NotNull()
    private java.util.Date fecha_devolucion;
    
    public final int getId() {
        return 0;
    }
    
    public final void setId(int p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.example.movieshow_store.models.Cliente getCliente() {
        return null;
    }
    
    public final void setCliente(@org.jetbrains.annotations.Nullable()
    com.example.movieshow_store.models.Cliente p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.example.movieshow_store.models.Cd getCd() {
        return null;
    }
    
    public final void setCd(@org.jetbrains.annotations.Nullable()
    com.example.movieshow_store.models.Cd p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.Date getFecha_prestamo() {
        return null;
    }
    
    public final void setFecha_prestamo(@org.jetbrains.annotations.NotNull()
    java.util.Date p0) {
    }
    
    public final float getPrecio() {
        return 0.0F;
    }
    
    public final void setPrecio(float p0) {
    }
    
    public final int getDias() {
        return 0;
    }
    
    public final void setDias(int p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.Date getFecha_devolucion() {
        return null;
    }
    
    public final void setFecha_devolucion(@org.jetbrains.annotations.NotNull()
    java.util.Date p0) {
    }
    
    public Alquiler(int id, @org.jetbrains.annotations.Nullable()
    com.example.movieshow_store.models.Cliente cliente, @org.jetbrains.annotations.Nullable()
    com.example.movieshow_store.models.Cd cd, @org.jetbrains.annotations.NotNull()
    java.util.Date fecha_prestamo, float precio, int dias, @org.jetbrains.annotations.NotNull()
    java.util.Date fecha_devolucion) {
        super();
    }
    
    public Alquiler() {
        super();
    }
}