package io.realm;


import android.annotation.TargetApi;
import android.os.Build;
import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.ImportFlag;
import io.realm.ProxyUtils;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.OsList;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.Property;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Row;
import io.realm.internal.Table;
import io.realm.internal.android.JsonUtils;
import io.realm.internal.objectstore.OsObjectBuilder;
import io.realm.log.RealmLog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressWarnings("all")
public class com_example_movieshow_store_models_CdRealmProxy extends com.example.movieshow_store.models.Cd
    implements RealmObjectProxy, com_example_movieshow_store_models_CdRealmProxyInterface {

    static final class CdColumnInfo extends ColumnInfo {
        long idColKey;
        long condicionColKey;
        long ubicacionColKey;
        long estadoColKey;

        CdColumnInfo(OsSchemaInfo schemaInfo) {
            super(4);
            OsObjectSchemaInfo objectSchemaInfo = schemaInfo.getObjectSchemaInfo("Cd");
            this.idColKey = addColumnDetails("id", "id", objectSchemaInfo);
            this.condicionColKey = addColumnDetails("condicion", "condicion", objectSchemaInfo);
            this.ubicacionColKey = addColumnDetails("ubicacion", "ubicacion", objectSchemaInfo);
            this.estadoColKey = addColumnDetails("estado", "estado", objectSchemaInfo);
        }

        CdColumnInfo(ColumnInfo src, boolean mutable) {
            super(src, mutable);
            copy(src, this);
        }

        @Override
        protected final ColumnInfo copy(boolean mutable) {
            return new CdColumnInfo(this, mutable);
        }

        @Override
        protected final void copy(ColumnInfo rawSrc, ColumnInfo rawDst) {
            final CdColumnInfo src = (CdColumnInfo) rawSrc;
            final CdColumnInfo dst = (CdColumnInfo) rawDst;
            dst.idColKey = src.idColKey;
            dst.condicionColKey = src.condicionColKey;
            dst.ubicacionColKey = src.ubicacionColKey;
            dst.estadoColKey = src.estadoColKey;
        }
    }

    private static final OsObjectSchemaInfo expectedObjectSchemaInfo = createExpectedObjectSchemaInfo();

    private CdColumnInfo columnInfo;
    private ProxyState<com.example.movieshow_store.models.Cd> proxyState;

    com_example_movieshow_store_models_CdRealmProxy() {
        proxyState.setConstructionFinished();
    }

    @Override
    public void realm$injectObjectContext() {
        if (this.proxyState != null) {
            return;
        }
        final BaseRealm.RealmObjectContext context = BaseRealm.objectContext.get();
        this.columnInfo = (CdColumnInfo) context.getColumnInfo();
        this.proxyState = new ProxyState<com.example.movieshow_store.models.Cd>(this);
        proxyState.setRealm$realm(context.getRealm());
        proxyState.setRow$realm(context.getRow());
        proxyState.setAcceptDefaultValue$realm(context.getAcceptDefaultValue());
        proxyState.setExcludeFields$realm(context.getExcludeFields());
    }

    @Override
    @SuppressWarnings("cast")
    public int realmGet$id() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.idColKey);
    }

    @Override
    public void realmSet$id(int value) {
        if (proxyState.isUnderConstruction()) {
            // default value of the primary key is always ignored.
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        throw new io.realm.exceptions.RealmException("Primary key field 'id' cannot be changed after object was created.");
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$condicion() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.condicionColKey);
    }

    @Override
    public void realmSet$condicion(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'condicion' to null.");
            }
            row.getTable().setString(columnInfo.condicionColKey, row.getObjectKey(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            throw new IllegalArgumentException("Trying to set non-nullable field 'condicion' to null.");
        }
        proxyState.getRow$realm().setString(columnInfo.condicionColKey, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$ubicacion() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.ubicacionColKey);
    }

    @Override
    public void realmSet$ubicacion(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'ubicacion' to null.");
            }
            row.getTable().setString(columnInfo.ubicacionColKey, row.getObjectKey(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            throw new IllegalArgumentException("Trying to set non-nullable field 'ubicacion' to null.");
        }
        proxyState.getRow$realm().setString(columnInfo.ubicacionColKey, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$estado() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.estadoColKey);
    }

    @Override
    public void realmSet$estado(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'estado' to null.");
            }
            row.getTable().setString(columnInfo.estadoColKey, row.getObjectKey(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            throw new IllegalArgumentException("Trying to set non-nullable field 'estado' to null.");
        }
        proxyState.getRow$realm().setString(columnInfo.estadoColKey, value);
    }

    private static OsObjectSchemaInfo createExpectedObjectSchemaInfo() {
        OsObjectSchemaInfo.Builder builder = new OsObjectSchemaInfo.Builder("Cd", 4, 0);
        builder.addPersistedProperty("id", RealmFieldType.INTEGER, Property.PRIMARY_KEY, Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("condicion", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("ubicacion", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("estado", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        return builder.build();
    }

    public static OsObjectSchemaInfo getExpectedObjectSchemaInfo() {
        return expectedObjectSchemaInfo;
    }

    public static CdColumnInfo createColumnInfo(OsSchemaInfo schemaInfo) {
        return new CdColumnInfo(schemaInfo);
    }

    public static String getSimpleClassName() {
        return "Cd";
    }

    public static final class ClassNameHelper {
        public static final String INTERNAL_CLASS_NAME = "Cd";
    }

    @SuppressWarnings("cast")
    public static com.example.movieshow_store.models.Cd createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        final List<String> excludeFields = Collections.<String> emptyList();
        com.example.movieshow_store.models.Cd obj = null;
        if (update) {
            Table table = realm.getTable(com.example.movieshow_store.models.Cd.class);
            CdColumnInfo columnInfo = (CdColumnInfo) realm.getSchema().getColumnInfo(com.example.movieshow_store.models.Cd.class);
            long pkColumnKey = columnInfo.idColKey;
            long colKey = Table.NO_MATCH;
            if (!json.isNull("id")) {
                colKey = table.findFirstLong(pkColumnKey, json.getLong("id"));
            }
            if (colKey != Table.NO_MATCH) {
                final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
                try {
                    objectContext.set(realm, table.getUncheckedRow(colKey), realm.getSchema().getColumnInfo(com.example.movieshow_store.models.Cd.class), false, Collections.<String> emptyList());
                    obj = new io.realm.com_example_movieshow_store_models_CdRealmProxy();
                } finally {
                    objectContext.clear();
                }
            }
        }
        if (obj == null) {
            if (json.has("id")) {
                if (json.isNull("id")) {
                    obj = (io.realm.com_example_movieshow_store_models_CdRealmProxy) realm.createObjectInternal(com.example.movieshow_store.models.Cd.class, null, true, excludeFields);
                } else {
                    obj = (io.realm.com_example_movieshow_store_models_CdRealmProxy) realm.createObjectInternal(com.example.movieshow_store.models.Cd.class, json.getInt("id"), true, excludeFields);
                }
            } else {
                throw new IllegalArgumentException("JSON object doesn't have the primary key field 'id'.");
            }
        }

        final com_example_movieshow_store_models_CdRealmProxyInterface objProxy = (com_example_movieshow_store_models_CdRealmProxyInterface) obj;
        if (json.has("condicion")) {
            if (json.isNull("condicion")) {
                objProxy.realmSet$condicion(null);
            } else {
                objProxy.realmSet$condicion((String) json.getString("condicion"));
            }
        }
        if (json.has("ubicacion")) {
            if (json.isNull("ubicacion")) {
                objProxy.realmSet$ubicacion(null);
            } else {
                objProxy.realmSet$ubicacion((String) json.getString("ubicacion"));
            }
        }
        if (json.has("estado")) {
            if (json.isNull("estado")) {
                objProxy.realmSet$estado(null);
            } else {
                objProxy.realmSet$estado((String) json.getString("estado"));
            }
        }
        return obj;
    }

    @SuppressWarnings("cast")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static com.example.movieshow_store.models.Cd createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        boolean jsonHasPrimaryKey = false;
        final com.example.movieshow_store.models.Cd obj = new com.example.movieshow_store.models.Cd();
        final com_example_movieshow_store_models_CdRealmProxyInterface objProxy = (com_example_movieshow_store_models_CdRealmProxyInterface) obj;
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (false) {
            } else if (name.equals("id")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$id((int) reader.nextInt());
                } else {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'id' to null.");
                }
                jsonHasPrimaryKey = true;
            } else if (name.equals("condicion")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$condicion((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$condicion(null);
                }
            } else if (name.equals("ubicacion")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$ubicacion((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$ubicacion(null);
                }
            } else if (name.equals("estado")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$estado((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$estado(null);
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        if (!jsonHasPrimaryKey) {
            throw new IllegalArgumentException("JSON object doesn't have the primary key field 'id'.");
        }
        return realm.copyToRealm(obj);
    }

    private static com_example_movieshow_store_models_CdRealmProxy newProxyInstance(BaseRealm realm, Row row) {
        // Ignore default values to avoid creating unexpected objects from RealmModel/RealmList fields
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        objectContext.set(realm, row, realm.getSchema().getColumnInfo(com.example.movieshow_store.models.Cd.class), false, Collections.<String>emptyList());
        io.realm.com_example_movieshow_store_models_CdRealmProxy obj = new io.realm.com_example_movieshow_store_models_CdRealmProxy();
        objectContext.clear();
        return obj;
    }

    public static com.example.movieshow_store.models.Cd copyOrUpdate(Realm realm, CdColumnInfo columnInfo, com.example.movieshow_store.models.Cd object, boolean update, Map<RealmModel,RealmObjectProxy> cache, Set<ImportFlag> flags) {
        if (object instanceof RealmObjectProxy && !RealmObject.isFrozen(object) && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null) {
            final BaseRealm otherRealm = ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm();
            if (otherRealm.threadId != realm.threadId) {
                throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
            }
            if (otherRealm.getPath().equals(realm.getPath())) {
                return object;
            }
        }
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        RealmObjectProxy cachedRealmObject = cache.get(object);
        if (cachedRealmObject != null) {
            return (com.example.movieshow_store.models.Cd) cachedRealmObject;
        }

        com.example.movieshow_store.models.Cd realmObject = null;
        boolean canUpdate = update;
        if (canUpdate) {
            Table table = realm.getTable(com.example.movieshow_store.models.Cd.class);
            long pkColumnKey = columnInfo.idColKey;
            long colKey = table.findFirstLong(pkColumnKey, ((com_example_movieshow_store_models_CdRealmProxyInterface) object).realmGet$id());
            if (colKey == Table.NO_MATCH) {
                canUpdate = false;
            } else {
                try {
                    objectContext.set(realm, table.getUncheckedRow(colKey), columnInfo, false, Collections.<String> emptyList());
                    realmObject = new io.realm.com_example_movieshow_store_models_CdRealmProxy();
                    cache.put(object, (RealmObjectProxy) realmObject);
                } finally {
                    objectContext.clear();
                }
            }
        }

        return (canUpdate) ? update(realm, columnInfo, realmObject, object, cache, flags) : copy(realm, columnInfo, object, update, cache, flags);
    }

    public static com.example.movieshow_store.models.Cd copy(Realm realm, CdColumnInfo columnInfo, com.example.movieshow_store.models.Cd newObject, boolean update, Map<RealmModel,RealmObjectProxy> cache, Set<ImportFlag> flags) {
        RealmObjectProxy cachedRealmObject = cache.get(newObject);
        if (cachedRealmObject != null) {
            return (com.example.movieshow_store.models.Cd) cachedRealmObject;
        }

        com_example_movieshow_store_models_CdRealmProxyInterface realmObjectSource = (com_example_movieshow_store_models_CdRealmProxyInterface) newObject;

        Table table = realm.getTable(com.example.movieshow_store.models.Cd.class);
        OsObjectBuilder builder = new OsObjectBuilder(table, flags);

        // Add all non-"object reference" fields
        builder.addInteger(columnInfo.idColKey, realmObjectSource.realmGet$id());
        builder.addString(columnInfo.condicionColKey, realmObjectSource.realmGet$condicion());
        builder.addString(columnInfo.ubicacionColKey, realmObjectSource.realmGet$ubicacion());
        builder.addString(columnInfo.estadoColKey, realmObjectSource.realmGet$estado());

        // Create the underlying object and cache it before setting any object/objectlist references
        // This will allow us to break any circular dependencies by using the object cache.
        Row row = builder.createNewObject();
        io.realm.com_example_movieshow_store_models_CdRealmProxy realmObjectCopy = newProxyInstance(realm, row);
        cache.put(newObject, realmObjectCopy);

        return realmObjectCopy;
    }

    public static long insert(Realm realm, com.example.movieshow_store.models.Cd object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && !RealmObject.isFrozen(object) && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getObjectKey();
        }
        Table table = realm.getTable(com.example.movieshow_store.models.Cd.class);
        long tableNativePtr = table.getNativePtr();
        CdColumnInfo columnInfo = (CdColumnInfo) realm.getSchema().getColumnInfo(com.example.movieshow_store.models.Cd.class);
        long pkColumnKey = columnInfo.idColKey;
        long colKey = Table.NO_MATCH;
        Object primaryKeyValue = ((com_example_movieshow_store_models_CdRealmProxyInterface) object).realmGet$id();
        if (primaryKeyValue != null) {
            colKey = Table.nativeFindFirstInt(tableNativePtr, pkColumnKey, ((com_example_movieshow_store_models_CdRealmProxyInterface) object).realmGet$id());
        }
        if (colKey == Table.NO_MATCH) {
            colKey = OsObject.createRowWithPrimaryKey(table, pkColumnKey, ((com_example_movieshow_store_models_CdRealmProxyInterface) object).realmGet$id());
        } else {
            Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
        }
        cache.put(object, colKey);
        String realmGet$condicion = ((com_example_movieshow_store_models_CdRealmProxyInterface) object).realmGet$condicion();
        if (realmGet$condicion != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.condicionColKey, colKey, realmGet$condicion, false);
        }
        String realmGet$ubicacion = ((com_example_movieshow_store_models_CdRealmProxyInterface) object).realmGet$ubicacion();
        if (realmGet$ubicacion != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.ubicacionColKey, colKey, realmGet$ubicacion, false);
        }
        String realmGet$estado = ((com_example_movieshow_store_models_CdRealmProxyInterface) object).realmGet$estado();
        if (realmGet$estado != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.estadoColKey, colKey, realmGet$estado, false);
        }
        return colKey;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.example.movieshow_store.models.Cd.class);
        long tableNativePtr = table.getNativePtr();
        CdColumnInfo columnInfo = (CdColumnInfo) realm.getSchema().getColumnInfo(com.example.movieshow_store.models.Cd.class);
        long pkColumnKey = columnInfo.idColKey;
        com.example.movieshow_store.models.Cd object = null;
        while (objects.hasNext()) {
            object = (com.example.movieshow_store.models.Cd) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && !RealmObject.isFrozen(object) && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getObjectKey());
                continue;
            }
            long colKey = Table.NO_MATCH;
            Object primaryKeyValue = ((com_example_movieshow_store_models_CdRealmProxyInterface) object).realmGet$id();
            if (primaryKeyValue != null) {
                colKey = Table.nativeFindFirstInt(tableNativePtr, pkColumnKey, ((com_example_movieshow_store_models_CdRealmProxyInterface) object).realmGet$id());
            }
            if (colKey == Table.NO_MATCH) {
                colKey = OsObject.createRowWithPrimaryKey(table, pkColumnKey, ((com_example_movieshow_store_models_CdRealmProxyInterface) object).realmGet$id());
            } else {
                Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
            }
            cache.put(object, colKey);
            String realmGet$condicion = ((com_example_movieshow_store_models_CdRealmProxyInterface) object).realmGet$condicion();
            if (realmGet$condicion != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.condicionColKey, colKey, realmGet$condicion, false);
            }
            String realmGet$ubicacion = ((com_example_movieshow_store_models_CdRealmProxyInterface) object).realmGet$ubicacion();
            if (realmGet$ubicacion != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.ubicacionColKey, colKey, realmGet$ubicacion, false);
            }
            String realmGet$estado = ((com_example_movieshow_store_models_CdRealmProxyInterface) object).realmGet$estado();
            if (realmGet$estado != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.estadoColKey, colKey, realmGet$estado, false);
            }
        }
    }

    public static long insertOrUpdate(Realm realm, com.example.movieshow_store.models.Cd object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && !RealmObject.isFrozen(object) && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getObjectKey();
        }
        Table table = realm.getTable(com.example.movieshow_store.models.Cd.class);
        long tableNativePtr = table.getNativePtr();
        CdColumnInfo columnInfo = (CdColumnInfo) realm.getSchema().getColumnInfo(com.example.movieshow_store.models.Cd.class);
        long pkColumnKey = columnInfo.idColKey;
        long colKey = Table.NO_MATCH;
        Object primaryKeyValue = ((com_example_movieshow_store_models_CdRealmProxyInterface) object).realmGet$id();
        if (primaryKeyValue != null) {
            colKey = Table.nativeFindFirstInt(tableNativePtr, pkColumnKey, ((com_example_movieshow_store_models_CdRealmProxyInterface) object).realmGet$id());
        }
        if (colKey == Table.NO_MATCH) {
            colKey = OsObject.createRowWithPrimaryKey(table, pkColumnKey, ((com_example_movieshow_store_models_CdRealmProxyInterface) object).realmGet$id());
        }
        cache.put(object, colKey);
        String realmGet$condicion = ((com_example_movieshow_store_models_CdRealmProxyInterface) object).realmGet$condicion();
        if (realmGet$condicion != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.condicionColKey, colKey, realmGet$condicion, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.condicionColKey, colKey, false);
        }
        String realmGet$ubicacion = ((com_example_movieshow_store_models_CdRealmProxyInterface) object).realmGet$ubicacion();
        if (realmGet$ubicacion != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.ubicacionColKey, colKey, realmGet$ubicacion, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.ubicacionColKey, colKey, false);
        }
        String realmGet$estado = ((com_example_movieshow_store_models_CdRealmProxyInterface) object).realmGet$estado();
        if (realmGet$estado != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.estadoColKey, colKey, realmGet$estado, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.estadoColKey, colKey, false);
        }
        return colKey;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.example.movieshow_store.models.Cd.class);
        long tableNativePtr = table.getNativePtr();
        CdColumnInfo columnInfo = (CdColumnInfo) realm.getSchema().getColumnInfo(com.example.movieshow_store.models.Cd.class);
        long pkColumnKey = columnInfo.idColKey;
        com.example.movieshow_store.models.Cd object = null;
        while (objects.hasNext()) {
            object = (com.example.movieshow_store.models.Cd) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && !RealmObject.isFrozen(object) && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getObjectKey());
                continue;
            }
            long colKey = Table.NO_MATCH;
            Object primaryKeyValue = ((com_example_movieshow_store_models_CdRealmProxyInterface) object).realmGet$id();
            if (primaryKeyValue != null) {
                colKey = Table.nativeFindFirstInt(tableNativePtr, pkColumnKey, ((com_example_movieshow_store_models_CdRealmProxyInterface) object).realmGet$id());
            }
            if (colKey == Table.NO_MATCH) {
                colKey = OsObject.createRowWithPrimaryKey(table, pkColumnKey, ((com_example_movieshow_store_models_CdRealmProxyInterface) object).realmGet$id());
            }
            cache.put(object, colKey);
            String realmGet$condicion = ((com_example_movieshow_store_models_CdRealmProxyInterface) object).realmGet$condicion();
            if (realmGet$condicion != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.condicionColKey, colKey, realmGet$condicion, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.condicionColKey, colKey, false);
            }
            String realmGet$ubicacion = ((com_example_movieshow_store_models_CdRealmProxyInterface) object).realmGet$ubicacion();
            if (realmGet$ubicacion != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.ubicacionColKey, colKey, realmGet$ubicacion, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.ubicacionColKey, colKey, false);
            }
            String realmGet$estado = ((com_example_movieshow_store_models_CdRealmProxyInterface) object).realmGet$estado();
            if (realmGet$estado != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.estadoColKey, colKey, realmGet$estado, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.estadoColKey, colKey, false);
            }
        }
    }

    public static com.example.movieshow_store.models.Cd createDetachedCopy(com.example.movieshow_store.models.Cd realmObject, int currentDepth, int maxDepth, Map<RealmModel, CacheData<RealmModel>> cache) {
        if (currentDepth > maxDepth || realmObject == null) {
            return null;
        }
        CacheData<RealmModel> cachedObject = cache.get(realmObject);
        com.example.movieshow_store.models.Cd unmanagedObject;
        if (cachedObject == null) {
            unmanagedObject = new com.example.movieshow_store.models.Cd();
            cache.put(realmObject, new RealmObjectProxy.CacheData<RealmModel>(currentDepth, unmanagedObject));
        } else {
            // Reuse cached object or recreate it because it was encountered at a lower depth.
            if (currentDepth >= cachedObject.minDepth) {
                return (com.example.movieshow_store.models.Cd) cachedObject.object;
            }
            unmanagedObject = (com.example.movieshow_store.models.Cd) cachedObject.object;
            cachedObject.minDepth = currentDepth;
        }
        com_example_movieshow_store_models_CdRealmProxyInterface unmanagedCopy = (com_example_movieshow_store_models_CdRealmProxyInterface) unmanagedObject;
        com_example_movieshow_store_models_CdRealmProxyInterface realmSource = (com_example_movieshow_store_models_CdRealmProxyInterface) realmObject;
        unmanagedCopy.realmSet$id(realmSource.realmGet$id());
        unmanagedCopy.realmSet$condicion(realmSource.realmGet$condicion());
        unmanagedCopy.realmSet$ubicacion(realmSource.realmGet$ubicacion());
        unmanagedCopy.realmSet$estado(realmSource.realmGet$estado());

        return unmanagedObject;
    }

    static com.example.movieshow_store.models.Cd update(Realm realm, CdColumnInfo columnInfo, com.example.movieshow_store.models.Cd realmObject, com.example.movieshow_store.models.Cd newObject, Map<RealmModel, RealmObjectProxy> cache, Set<ImportFlag> flags) {
        com_example_movieshow_store_models_CdRealmProxyInterface realmObjectTarget = (com_example_movieshow_store_models_CdRealmProxyInterface) realmObject;
        com_example_movieshow_store_models_CdRealmProxyInterface realmObjectSource = (com_example_movieshow_store_models_CdRealmProxyInterface) newObject;
        Table table = realm.getTable(com.example.movieshow_store.models.Cd.class);
        OsObjectBuilder builder = new OsObjectBuilder(table, flags);
        builder.addInteger(columnInfo.idColKey, realmObjectSource.realmGet$id());
        builder.addString(columnInfo.condicionColKey, realmObjectSource.realmGet$condicion());
        builder.addString(columnInfo.ubicacionColKey, realmObjectSource.realmGet$ubicacion());
        builder.addString(columnInfo.estadoColKey, realmObjectSource.realmGet$estado());

        builder.updateExistingObject();
        return realmObject;
    }

    @Override
    @SuppressWarnings("ArrayToString")
    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("Cd = proxy[");
        stringBuilder.append("{id:");
        stringBuilder.append(realmGet$id());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{condicion:");
        stringBuilder.append(realmGet$condicion());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{ubicacion:");
        stringBuilder.append(realmGet$ubicacion());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{estado:");
        stringBuilder.append(realmGet$estado());
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public ProxyState<?> realmGet$proxyState() {
        return proxyState;
    }

    @Override
    public int hashCode() {
        String realmName = proxyState.getRealm$realm().getPath();
        String tableName = proxyState.getRow$realm().getTable().getName();
        long colKey = proxyState.getRow$realm().getObjectKey();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (colKey ^ (colKey >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        com_example_movieshow_store_models_CdRealmProxy aCd = (com_example_movieshow_store_models_CdRealmProxy)o;

        BaseRealm realm = proxyState.getRealm$realm();
        BaseRealm otherRealm = aCd.proxyState.getRealm$realm();
        String path = realm.getPath();
        String otherPath = otherRealm.getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;
        if (realm.isFrozen() != otherRealm.isFrozen()) return false;
        if (!realm.sharedRealm.getVersionID().equals(otherRealm.sharedRealm.getVersionID())) {
            return false;
        }

        String tableName = proxyState.getRow$realm().getTable().getName();
        String otherTableName = aCd.proxyState.getRow$realm().getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (proxyState.getRow$realm().getObjectKey() != aCd.proxyState.getRow$realm().getObjectKey()) return false;

        return true;
    }
}
