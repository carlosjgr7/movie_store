package io.realm;


public interface com_example_movieshow_store_models_SansionRealmProxyInterface {
    public int realmGet$id();
    public void realmSet$id(int value);
    public com.example.movieshow_store.models.Cliente realmGet$cliente();
    public void realmSet$cliente(com.example.movieshow_store.models.Cliente value);
    public com.example.movieshow_store.models.Alquiler realmGet$alquiler();
    public void realmSet$alquiler(com.example.movieshow_store.models.Alquiler value);
    public int realmGet$tipo();
    public void realmSet$tipo(int value);
    public int realmGet$dias();
    public void realmSet$dias(int value);
}
