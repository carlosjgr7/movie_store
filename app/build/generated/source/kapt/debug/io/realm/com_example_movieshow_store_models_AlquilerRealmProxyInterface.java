package io.realm;


public interface com_example_movieshow_store_models_AlquilerRealmProxyInterface {
    public int realmGet$id();
    public void realmSet$id(int value);
    public com.example.movieshow_store.models.Cliente realmGet$cliente();
    public void realmSet$cliente(com.example.movieshow_store.models.Cliente value);
    public com.example.movieshow_store.models.Cd realmGet$cd();
    public void realmSet$cd(com.example.movieshow_store.models.Cd value);
    public java.util.Date realmGet$fecha_prestamo();
    public void realmSet$fecha_prestamo(java.util.Date value);
    public float realmGet$precio();
    public void realmSet$precio(float value);
    public int realmGet$dias();
    public void realmSet$dias(int value);
    public java.util.Date realmGet$fecha_devolucion();
    public void realmSet$fecha_devolucion(java.util.Date value);
}
