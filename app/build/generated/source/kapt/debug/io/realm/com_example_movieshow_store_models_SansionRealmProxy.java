package io.realm;


import android.annotation.TargetApi;
import android.os.Build;
import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.ImportFlag;
import io.realm.ProxyUtils;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.OsList;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.Property;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Row;
import io.realm.internal.Table;
import io.realm.internal.android.JsonUtils;
import io.realm.internal.objectstore.OsObjectBuilder;
import io.realm.log.RealmLog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressWarnings("all")
public class com_example_movieshow_store_models_SansionRealmProxy extends com.example.movieshow_store.models.Sansion
    implements RealmObjectProxy, com_example_movieshow_store_models_SansionRealmProxyInterface {

    static final class SansionColumnInfo extends ColumnInfo {
        long idColKey;
        long clienteColKey;
        long alquilerColKey;
        long tipoColKey;
        long diasColKey;

        SansionColumnInfo(OsSchemaInfo schemaInfo) {
            super(5);
            OsObjectSchemaInfo objectSchemaInfo = schemaInfo.getObjectSchemaInfo("Sansion");
            this.idColKey = addColumnDetails("id", "id", objectSchemaInfo);
            this.clienteColKey = addColumnDetails("cliente", "cliente", objectSchemaInfo);
            this.alquilerColKey = addColumnDetails("alquiler", "alquiler", objectSchemaInfo);
            this.tipoColKey = addColumnDetails("tipo", "tipo", objectSchemaInfo);
            this.diasColKey = addColumnDetails("dias", "dias", objectSchemaInfo);
        }

        SansionColumnInfo(ColumnInfo src, boolean mutable) {
            super(src, mutable);
            copy(src, this);
        }

        @Override
        protected final ColumnInfo copy(boolean mutable) {
            return new SansionColumnInfo(this, mutable);
        }

        @Override
        protected final void copy(ColumnInfo rawSrc, ColumnInfo rawDst) {
            final SansionColumnInfo src = (SansionColumnInfo) rawSrc;
            final SansionColumnInfo dst = (SansionColumnInfo) rawDst;
            dst.idColKey = src.idColKey;
            dst.clienteColKey = src.clienteColKey;
            dst.alquilerColKey = src.alquilerColKey;
            dst.tipoColKey = src.tipoColKey;
            dst.diasColKey = src.diasColKey;
        }
    }

    private static final OsObjectSchemaInfo expectedObjectSchemaInfo = createExpectedObjectSchemaInfo();

    private SansionColumnInfo columnInfo;
    private ProxyState<com.example.movieshow_store.models.Sansion> proxyState;

    com_example_movieshow_store_models_SansionRealmProxy() {
        proxyState.setConstructionFinished();
    }

    @Override
    public void realm$injectObjectContext() {
        if (this.proxyState != null) {
            return;
        }
        final BaseRealm.RealmObjectContext context = BaseRealm.objectContext.get();
        this.columnInfo = (SansionColumnInfo) context.getColumnInfo();
        this.proxyState = new ProxyState<com.example.movieshow_store.models.Sansion>(this);
        proxyState.setRealm$realm(context.getRealm());
        proxyState.setRow$realm(context.getRow());
        proxyState.setAcceptDefaultValue$realm(context.getAcceptDefaultValue());
        proxyState.setExcludeFields$realm(context.getExcludeFields());
    }

    @Override
    @SuppressWarnings("cast")
    public int realmGet$id() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.idColKey);
    }

    @Override
    public void realmSet$id(int value) {
        if (proxyState.isUnderConstruction()) {
            // default value of the primary key is always ignored.
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        throw new io.realm.exceptions.RealmException("Primary key field 'id' cannot be changed after object was created.");
    }

    @Override
    public com.example.movieshow_store.models.Cliente realmGet$cliente() {
        proxyState.getRealm$realm().checkIfValid();
        if (proxyState.getRow$realm().isNullLink(columnInfo.clienteColKey)) {
            return null;
        }
        return proxyState.getRealm$realm().get(com.example.movieshow_store.models.Cliente.class, proxyState.getRow$realm().getLink(columnInfo.clienteColKey), false, Collections.<String>emptyList());
    }

    @Override
    public void realmSet$cliente(com.example.movieshow_store.models.Cliente value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            if (proxyState.getExcludeFields$realm().contains("cliente")) {
                return;
            }
            if (value != null && !RealmObject.isManaged(value)) {
                value = ((Realm) proxyState.getRealm$realm()).copyToRealm(value);
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                // Table#nullifyLink() does not support default value. Just using Row.
                row.nullifyLink(columnInfo.clienteColKey);
                return;
            }
            proxyState.checkValidObject(value);
            row.getTable().setLink(columnInfo.clienteColKey, row.getObjectKey(), ((RealmObjectProxy) value).realmGet$proxyState().getRow$realm().getObjectKey(), true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().nullifyLink(columnInfo.clienteColKey);
            return;
        }
        proxyState.checkValidObject(value);
        proxyState.getRow$realm().setLink(columnInfo.clienteColKey, ((RealmObjectProxy) value).realmGet$proxyState().getRow$realm().getObjectKey());
    }

    @Override
    public com.example.movieshow_store.models.Alquiler realmGet$alquiler() {
        proxyState.getRealm$realm().checkIfValid();
        if (proxyState.getRow$realm().isNullLink(columnInfo.alquilerColKey)) {
            return null;
        }
        return proxyState.getRealm$realm().get(com.example.movieshow_store.models.Alquiler.class, proxyState.getRow$realm().getLink(columnInfo.alquilerColKey), false, Collections.<String>emptyList());
    }

    @Override
    public void realmSet$alquiler(com.example.movieshow_store.models.Alquiler value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            if (proxyState.getExcludeFields$realm().contains("alquiler")) {
                return;
            }
            if (value != null && !RealmObject.isManaged(value)) {
                value = ((Realm) proxyState.getRealm$realm()).copyToRealm(value);
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                // Table#nullifyLink() does not support default value. Just using Row.
                row.nullifyLink(columnInfo.alquilerColKey);
                return;
            }
            proxyState.checkValidObject(value);
            row.getTable().setLink(columnInfo.alquilerColKey, row.getObjectKey(), ((RealmObjectProxy) value).realmGet$proxyState().getRow$realm().getObjectKey(), true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().nullifyLink(columnInfo.alquilerColKey);
            return;
        }
        proxyState.checkValidObject(value);
        proxyState.getRow$realm().setLink(columnInfo.alquilerColKey, ((RealmObjectProxy) value).realmGet$proxyState().getRow$realm().getObjectKey());
    }

    @Override
    @SuppressWarnings("cast")
    public int realmGet$tipo() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.tipoColKey);
    }

    @Override
    public void realmSet$tipo(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.tipoColKey, row.getObjectKey(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.tipoColKey, value);
    }

    @Override
    @SuppressWarnings("cast")
    public int realmGet$dias() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.diasColKey);
    }

    @Override
    public void realmSet$dias(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.diasColKey, row.getObjectKey(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.diasColKey, value);
    }

    private static OsObjectSchemaInfo createExpectedObjectSchemaInfo() {
        OsObjectSchemaInfo.Builder builder = new OsObjectSchemaInfo.Builder("Sansion", 5, 0);
        builder.addPersistedProperty("id", RealmFieldType.INTEGER, Property.PRIMARY_KEY, Property.INDEXED, Property.REQUIRED);
        builder.addPersistedLinkProperty("cliente", RealmFieldType.OBJECT, "Cliente");
        builder.addPersistedLinkProperty("alquiler", RealmFieldType.OBJECT, "Alquiler");
        builder.addPersistedProperty("tipo", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("dias", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        return builder.build();
    }

    public static OsObjectSchemaInfo getExpectedObjectSchemaInfo() {
        return expectedObjectSchemaInfo;
    }

    public static SansionColumnInfo createColumnInfo(OsSchemaInfo schemaInfo) {
        return new SansionColumnInfo(schemaInfo);
    }

    public static String getSimpleClassName() {
        return "Sansion";
    }

    public static final class ClassNameHelper {
        public static final String INTERNAL_CLASS_NAME = "Sansion";
    }

    @SuppressWarnings("cast")
    public static com.example.movieshow_store.models.Sansion createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        final List<String> excludeFields = new ArrayList<String>(2);
        com.example.movieshow_store.models.Sansion obj = null;
        if (update) {
            Table table = realm.getTable(com.example.movieshow_store.models.Sansion.class);
            SansionColumnInfo columnInfo = (SansionColumnInfo) realm.getSchema().getColumnInfo(com.example.movieshow_store.models.Sansion.class);
            long pkColumnKey = columnInfo.idColKey;
            long colKey = Table.NO_MATCH;
            if (!json.isNull("id")) {
                colKey = table.findFirstLong(pkColumnKey, json.getLong("id"));
            }
            if (colKey != Table.NO_MATCH) {
                final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
                try {
                    objectContext.set(realm, table.getUncheckedRow(colKey), realm.getSchema().getColumnInfo(com.example.movieshow_store.models.Sansion.class), false, Collections.<String> emptyList());
                    obj = new io.realm.com_example_movieshow_store_models_SansionRealmProxy();
                } finally {
                    objectContext.clear();
                }
            }
        }
        if (obj == null) {
            if (json.has("cliente")) {
                excludeFields.add("cliente");
            }
            if (json.has("alquiler")) {
                excludeFields.add("alquiler");
            }
            if (json.has("id")) {
                if (json.isNull("id")) {
                    obj = (io.realm.com_example_movieshow_store_models_SansionRealmProxy) realm.createObjectInternal(com.example.movieshow_store.models.Sansion.class, null, true, excludeFields);
                } else {
                    obj = (io.realm.com_example_movieshow_store_models_SansionRealmProxy) realm.createObjectInternal(com.example.movieshow_store.models.Sansion.class, json.getInt("id"), true, excludeFields);
                }
            } else {
                throw new IllegalArgumentException("JSON object doesn't have the primary key field 'id'.");
            }
        }

        final com_example_movieshow_store_models_SansionRealmProxyInterface objProxy = (com_example_movieshow_store_models_SansionRealmProxyInterface) obj;
        if (json.has("cliente")) {
            if (json.isNull("cliente")) {
                objProxy.realmSet$cliente(null);
            } else {
                com.example.movieshow_store.models.Cliente clienteObj = com_example_movieshow_store_models_ClienteRealmProxy.createOrUpdateUsingJsonObject(realm, json.getJSONObject("cliente"), update);
                objProxy.realmSet$cliente(clienteObj);
            }
        }
        if (json.has("alquiler")) {
            if (json.isNull("alquiler")) {
                objProxy.realmSet$alquiler(null);
            } else {
                com.example.movieshow_store.models.Alquiler alquilerObj = com_example_movieshow_store_models_AlquilerRealmProxy.createOrUpdateUsingJsonObject(realm, json.getJSONObject("alquiler"), update);
                objProxy.realmSet$alquiler(alquilerObj);
            }
        }
        if (json.has("tipo")) {
            if (json.isNull("tipo")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'tipo' to null.");
            } else {
                objProxy.realmSet$tipo((int) json.getInt("tipo"));
            }
        }
        if (json.has("dias")) {
            if (json.isNull("dias")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'dias' to null.");
            } else {
                objProxy.realmSet$dias((int) json.getInt("dias"));
            }
        }
        return obj;
    }

    @SuppressWarnings("cast")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static com.example.movieshow_store.models.Sansion createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        boolean jsonHasPrimaryKey = false;
        final com.example.movieshow_store.models.Sansion obj = new com.example.movieshow_store.models.Sansion();
        final com_example_movieshow_store_models_SansionRealmProxyInterface objProxy = (com_example_movieshow_store_models_SansionRealmProxyInterface) obj;
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (false) {
            } else if (name.equals("id")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$id((int) reader.nextInt());
                } else {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'id' to null.");
                }
                jsonHasPrimaryKey = true;
            } else if (name.equals("cliente")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    objProxy.realmSet$cliente(null);
                } else {
                    com.example.movieshow_store.models.Cliente clienteObj = com_example_movieshow_store_models_ClienteRealmProxy.createUsingJsonStream(realm, reader);
                    objProxy.realmSet$cliente(clienteObj);
                }
            } else if (name.equals("alquiler")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    objProxy.realmSet$alquiler(null);
                } else {
                    com.example.movieshow_store.models.Alquiler alquilerObj = com_example_movieshow_store_models_AlquilerRealmProxy.createUsingJsonStream(realm, reader);
                    objProxy.realmSet$alquiler(alquilerObj);
                }
            } else if (name.equals("tipo")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$tipo((int) reader.nextInt());
                } else {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'tipo' to null.");
                }
            } else if (name.equals("dias")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$dias((int) reader.nextInt());
                } else {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'dias' to null.");
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        if (!jsonHasPrimaryKey) {
            throw new IllegalArgumentException("JSON object doesn't have the primary key field 'id'.");
        }
        return realm.copyToRealm(obj);
    }

    private static com_example_movieshow_store_models_SansionRealmProxy newProxyInstance(BaseRealm realm, Row row) {
        // Ignore default values to avoid creating unexpected objects from RealmModel/RealmList fields
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        objectContext.set(realm, row, realm.getSchema().getColumnInfo(com.example.movieshow_store.models.Sansion.class), false, Collections.<String>emptyList());
        io.realm.com_example_movieshow_store_models_SansionRealmProxy obj = new io.realm.com_example_movieshow_store_models_SansionRealmProxy();
        objectContext.clear();
        return obj;
    }

    public static com.example.movieshow_store.models.Sansion copyOrUpdate(Realm realm, SansionColumnInfo columnInfo, com.example.movieshow_store.models.Sansion object, boolean update, Map<RealmModel,RealmObjectProxy> cache, Set<ImportFlag> flags) {
        if (object instanceof RealmObjectProxy && !RealmObject.isFrozen(object) && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null) {
            final BaseRealm otherRealm = ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm();
            if (otherRealm.threadId != realm.threadId) {
                throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
            }
            if (otherRealm.getPath().equals(realm.getPath())) {
                return object;
            }
        }
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        RealmObjectProxy cachedRealmObject = cache.get(object);
        if (cachedRealmObject != null) {
            return (com.example.movieshow_store.models.Sansion) cachedRealmObject;
        }

        com.example.movieshow_store.models.Sansion realmObject = null;
        boolean canUpdate = update;
        if (canUpdate) {
            Table table = realm.getTable(com.example.movieshow_store.models.Sansion.class);
            long pkColumnKey = columnInfo.idColKey;
            long colKey = table.findFirstLong(pkColumnKey, ((com_example_movieshow_store_models_SansionRealmProxyInterface) object).realmGet$id());
            if (colKey == Table.NO_MATCH) {
                canUpdate = false;
            } else {
                try {
                    objectContext.set(realm, table.getUncheckedRow(colKey), columnInfo, false, Collections.<String> emptyList());
                    realmObject = new io.realm.com_example_movieshow_store_models_SansionRealmProxy();
                    cache.put(object, (RealmObjectProxy) realmObject);
                } finally {
                    objectContext.clear();
                }
            }
        }

        return (canUpdate) ? update(realm, columnInfo, realmObject, object, cache, flags) : copy(realm, columnInfo, object, update, cache, flags);
    }

    public static com.example.movieshow_store.models.Sansion copy(Realm realm, SansionColumnInfo columnInfo, com.example.movieshow_store.models.Sansion newObject, boolean update, Map<RealmModel,RealmObjectProxy> cache, Set<ImportFlag> flags) {
        RealmObjectProxy cachedRealmObject = cache.get(newObject);
        if (cachedRealmObject != null) {
            return (com.example.movieshow_store.models.Sansion) cachedRealmObject;
        }

        com_example_movieshow_store_models_SansionRealmProxyInterface realmObjectSource = (com_example_movieshow_store_models_SansionRealmProxyInterface) newObject;

        Table table = realm.getTable(com.example.movieshow_store.models.Sansion.class);
        OsObjectBuilder builder = new OsObjectBuilder(table, flags);

        // Add all non-"object reference" fields
        builder.addInteger(columnInfo.idColKey, realmObjectSource.realmGet$id());
        builder.addInteger(columnInfo.tipoColKey, realmObjectSource.realmGet$tipo());
        builder.addInteger(columnInfo.diasColKey, realmObjectSource.realmGet$dias());

        // Create the underlying object and cache it before setting any object/objectlist references
        // This will allow us to break any circular dependencies by using the object cache.
        Row row = builder.createNewObject();
        io.realm.com_example_movieshow_store_models_SansionRealmProxy realmObjectCopy = newProxyInstance(realm, row);
        cache.put(newObject, realmObjectCopy);

        // Finally add all fields that reference other Realm Objects, either directly or through a list
        com.example.movieshow_store.models.Cliente clienteObj = realmObjectSource.realmGet$cliente();
        if (clienteObj == null) {
            realmObjectCopy.realmSet$cliente(null);
        } else {
            com.example.movieshow_store.models.Cliente cachecliente = (com.example.movieshow_store.models.Cliente) cache.get(clienteObj);
            if (cachecliente != null) {
                realmObjectCopy.realmSet$cliente(cachecliente);
            } else {
                realmObjectCopy.realmSet$cliente(com_example_movieshow_store_models_ClienteRealmProxy.copyOrUpdate(realm, (com_example_movieshow_store_models_ClienteRealmProxy.ClienteColumnInfo) realm.getSchema().getColumnInfo(com.example.movieshow_store.models.Cliente.class), clienteObj, update, cache, flags));
            }
        }

        com.example.movieshow_store.models.Alquiler alquilerObj = realmObjectSource.realmGet$alquiler();
        if (alquilerObj == null) {
            realmObjectCopy.realmSet$alquiler(null);
        } else {
            com.example.movieshow_store.models.Alquiler cachealquiler = (com.example.movieshow_store.models.Alquiler) cache.get(alquilerObj);
            if (cachealquiler != null) {
                realmObjectCopy.realmSet$alquiler(cachealquiler);
            } else {
                realmObjectCopy.realmSet$alquiler(com_example_movieshow_store_models_AlquilerRealmProxy.copyOrUpdate(realm, (com_example_movieshow_store_models_AlquilerRealmProxy.AlquilerColumnInfo) realm.getSchema().getColumnInfo(com.example.movieshow_store.models.Alquiler.class), alquilerObj, update, cache, flags));
            }
        }

        return realmObjectCopy;
    }

    public static long insert(Realm realm, com.example.movieshow_store.models.Sansion object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && !RealmObject.isFrozen(object) && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getObjectKey();
        }
        Table table = realm.getTable(com.example.movieshow_store.models.Sansion.class);
        long tableNativePtr = table.getNativePtr();
        SansionColumnInfo columnInfo = (SansionColumnInfo) realm.getSchema().getColumnInfo(com.example.movieshow_store.models.Sansion.class);
        long pkColumnKey = columnInfo.idColKey;
        long colKey = Table.NO_MATCH;
        Object primaryKeyValue = ((com_example_movieshow_store_models_SansionRealmProxyInterface) object).realmGet$id();
        if (primaryKeyValue != null) {
            colKey = Table.nativeFindFirstInt(tableNativePtr, pkColumnKey, ((com_example_movieshow_store_models_SansionRealmProxyInterface) object).realmGet$id());
        }
        if (colKey == Table.NO_MATCH) {
            colKey = OsObject.createRowWithPrimaryKey(table, pkColumnKey, ((com_example_movieshow_store_models_SansionRealmProxyInterface) object).realmGet$id());
        } else {
            Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
        }
        cache.put(object, colKey);

        com.example.movieshow_store.models.Cliente clienteObj = ((com_example_movieshow_store_models_SansionRealmProxyInterface) object).realmGet$cliente();
        if (clienteObj != null) {
            Long cachecliente = cache.get(clienteObj);
            if (cachecliente == null) {
                cachecliente = com_example_movieshow_store_models_ClienteRealmProxy.insert(realm, clienteObj, cache);
            }
            Table.nativeSetLink(tableNativePtr, columnInfo.clienteColKey, colKey, cachecliente, false);
        }

        com.example.movieshow_store.models.Alquiler alquilerObj = ((com_example_movieshow_store_models_SansionRealmProxyInterface) object).realmGet$alquiler();
        if (alquilerObj != null) {
            Long cachealquiler = cache.get(alquilerObj);
            if (cachealquiler == null) {
                cachealquiler = com_example_movieshow_store_models_AlquilerRealmProxy.insert(realm, alquilerObj, cache);
            }
            Table.nativeSetLink(tableNativePtr, columnInfo.alquilerColKey, colKey, cachealquiler, false);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.tipoColKey, colKey, ((com_example_movieshow_store_models_SansionRealmProxyInterface) object).realmGet$tipo(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.diasColKey, colKey, ((com_example_movieshow_store_models_SansionRealmProxyInterface) object).realmGet$dias(), false);
        return colKey;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.example.movieshow_store.models.Sansion.class);
        long tableNativePtr = table.getNativePtr();
        SansionColumnInfo columnInfo = (SansionColumnInfo) realm.getSchema().getColumnInfo(com.example.movieshow_store.models.Sansion.class);
        long pkColumnKey = columnInfo.idColKey;
        com.example.movieshow_store.models.Sansion object = null;
        while (objects.hasNext()) {
            object = (com.example.movieshow_store.models.Sansion) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && !RealmObject.isFrozen(object) && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getObjectKey());
                continue;
            }
            long colKey = Table.NO_MATCH;
            Object primaryKeyValue = ((com_example_movieshow_store_models_SansionRealmProxyInterface) object).realmGet$id();
            if (primaryKeyValue != null) {
                colKey = Table.nativeFindFirstInt(tableNativePtr, pkColumnKey, ((com_example_movieshow_store_models_SansionRealmProxyInterface) object).realmGet$id());
            }
            if (colKey == Table.NO_MATCH) {
                colKey = OsObject.createRowWithPrimaryKey(table, pkColumnKey, ((com_example_movieshow_store_models_SansionRealmProxyInterface) object).realmGet$id());
            } else {
                Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
            }
            cache.put(object, colKey);

            com.example.movieshow_store.models.Cliente clienteObj = ((com_example_movieshow_store_models_SansionRealmProxyInterface) object).realmGet$cliente();
            if (clienteObj != null) {
                Long cachecliente = cache.get(clienteObj);
                if (cachecliente == null) {
                    cachecliente = com_example_movieshow_store_models_ClienteRealmProxy.insert(realm, clienteObj, cache);
                }
                table.setLink(columnInfo.clienteColKey, colKey, cachecliente, false);
            }

            com.example.movieshow_store.models.Alquiler alquilerObj = ((com_example_movieshow_store_models_SansionRealmProxyInterface) object).realmGet$alquiler();
            if (alquilerObj != null) {
                Long cachealquiler = cache.get(alquilerObj);
                if (cachealquiler == null) {
                    cachealquiler = com_example_movieshow_store_models_AlquilerRealmProxy.insert(realm, alquilerObj, cache);
                }
                table.setLink(columnInfo.alquilerColKey, colKey, cachealquiler, false);
            }
            Table.nativeSetLong(tableNativePtr, columnInfo.tipoColKey, colKey, ((com_example_movieshow_store_models_SansionRealmProxyInterface) object).realmGet$tipo(), false);
            Table.nativeSetLong(tableNativePtr, columnInfo.diasColKey, colKey, ((com_example_movieshow_store_models_SansionRealmProxyInterface) object).realmGet$dias(), false);
        }
    }

    public static long insertOrUpdate(Realm realm, com.example.movieshow_store.models.Sansion object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && !RealmObject.isFrozen(object) && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getObjectKey();
        }
        Table table = realm.getTable(com.example.movieshow_store.models.Sansion.class);
        long tableNativePtr = table.getNativePtr();
        SansionColumnInfo columnInfo = (SansionColumnInfo) realm.getSchema().getColumnInfo(com.example.movieshow_store.models.Sansion.class);
        long pkColumnKey = columnInfo.idColKey;
        long colKey = Table.NO_MATCH;
        Object primaryKeyValue = ((com_example_movieshow_store_models_SansionRealmProxyInterface) object).realmGet$id();
        if (primaryKeyValue != null) {
            colKey = Table.nativeFindFirstInt(tableNativePtr, pkColumnKey, ((com_example_movieshow_store_models_SansionRealmProxyInterface) object).realmGet$id());
        }
        if (colKey == Table.NO_MATCH) {
            colKey = OsObject.createRowWithPrimaryKey(table, pkColumnKey, ((com_example_movieshow_store_models_SansionRealmProxyInterface) object).realmGet$id());
        }
        cache.put(object, colKey);

        com.example.movieshow_store.models.Cliente clienteObj = ((com_example_movieshow_store_models_SansionRealmProxyInterface) object).realmGet$cliente();
        if (clienteObj != null) {
            Long cachecliente = cache.get(clienteObj);
            if (cachecliente == null) {
                cachecliente = com_example_movieshow_store_models_ClienteRealmProxy.insertOrUpdate(realm, clienteObj, cache);
            }
            Table.nativeSetLink(tableNativePtr, columnInfo.clienteColKey, colKey, cachecliente, false);
        } else {
            Table.nativeNullifyLink(tableNativePtr, columnInfo.clienteColKey, colKey);
        }

        com.example.movieshow_store.models.Alquiler alquilerObj = ((com_example_movieshow_store_models_SansionRealmProxyInterface) object).realmGet$alquiler();
        if (alquilerObj != null) {
            Long cachealquiler = cache.get(alquilerObj);
            if (cachealquiler == null) {
                cachealquiler = com_example_movieshow_store_models_AlquilerRealmProxy.insertOrUpdate(realm, alquilerObj, cache);
            }
            Table.nativeSetLink(tableNativePtr, columnInfo.alquilerColKey, colKey, cachealquiler, false);
        } else {
            Table.nativeNullifyLink(tableNativePtr, columnInfo.alquilerColKey, colKey);
        }
        Table.nativeSetLong(tableNativePtr, columnInfo.tipoColKey, colKey, ((com_example_movieshow_store_models_SansionRealmProxyInterface) object).realmGet$tipo(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.diasColKey, colKey, ((com_example_movieshow_store_models_SansionRealmProxyInterface) object).realmGet$dias(), false);
        return colKey;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.example.movieshow_store.models.Sansion.class);
        long tableNativePtr = table.getNativePtr();
        SansionColumnInfo columnInfo = (SansionColumnInfo) realm.getSchema().getColumnInfo(com.example.movieshow_store.models.Sansion.class);
        long pkColumnKey = columnInfo.idColKey;
        com.example.movieshow_store.models.Sansion object = null;
        while (objects.hasNext()) {
            object = (com.example.movieshow_store.models.Sansion) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && !RealmObject.isFrozen(object) && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getObjectKey());
                continue;
            }
            long colKey = Table.NO_MATCH;
            Object primaryKeyValue = ((com_example_movieshow_store_models_SansionRealmProxyInterface) object).realmGet$id();
            if (primaryKeyValue != null) {
                colKey = Table.nativeFindFirstInt(tableNativePtr, pkColumnKey, ((com_example_movieshow_store_models_SansionRealmProxyInterface) object).realmGet$id());
            }
            if (colKey == Table.NO_MATCH) {
                colKey = OsObject.createRowWithPrimaryKey(table, pkColumnKey, ((com_example_movieshow_store_models_SansionRealmProxyInterface) object).realmGet$id());
            }
            cache.put(object, colKey);

            com.example.movieshow_store.models.Cliente clienteObj = ((com_example_movieshow_store_models_SansionRealmProxyInterface) object).realmGet$cliente();
            if (clienteObj != null) {
                Long cachecliente = cache.get(clienteObj);
                if (cachecliente == null) {
                    cachecliente = com_example_movieshow_store_models_ClienteRealmProxy.insertOrUpdate(realm, clienteObj, cache);
                }
                Table.nativeSetLink(tableNativePtr, columnInfo.clienteColKey, colKey, cachecliente, false);
            } else {
                Table.nativeNullifyLink(tableNativePtr, columnInfo.clienteColKey, colKey);
            }

            com.example.movieshow_store.models.Alquiler alquilerObj = ((com_example_movieshow_store_models_SansionRealmProxyInterface) object).realmGet$alquiler();
            if (alquilerObj != null) {
                Long cachealquiler = cache.get(alquilerObj);
                if (cachealquiler == null) {
                    cachealquiler = com_example_movieshow_store_models_AlquilerRealmProxy.insertOrUpdate(realm, alquilerObj, cache);
                }
                Table.nativeSetLink(tableNativePtr, columnInfo.alquilerColKey, colKey, cachealquiler, false);
            } else {
                Table.nativeNullifyLink(tableNativePtr, columnInfo.alquilerColKey, colKey);
            }
            Table.nativeSetLong(tableNativePtr, columnInfo.tipoColKey, colKey, ((com_example_movieshow_store_models_SansionRealmProxyInterface) object).realmGet$tipo(), false);
            Table.nativeSetLong(tableNativePtr, columnInfo.diasColKey, colKey, ((com_example_movieshow_store_models_SansionRealmProxyInterface) object).realmGet$dias(), false);
        }
    }

    public static com.example.movieshow_store.models.Sansion createDetachedCopy(com.example.movieshow_store.models.Sansion realmObject, int currentDepth, int maxDepth, Map<RealmModel, CacheData<RealmModel>> cache) {
        if (currentDepth > maxDepth || realmObject == null) {
            return null;
        }
        CacheData<RealmModel> cachedObject = cache.get(realmObject);
        com.example.movieshow_store.models.Sansion unmanagedObject;
        if (cachedObject == null) {
            unmanagedObject = new com.example.movieshow_store.models.Sansion();
            cache.put(realmObject, new RealmObjectProxy.CacheData<RealmModel>(currentDepth, unmanagedObject));
        } else {
            // Reuse cached object or recreate it because it was encountered at a lower depth.
            if (currentDepth >= cachedObject.minDepth) {
                return (com.example.movieshow_store.models.Sansion) cachedObject.object;
            }
            unmanagedObject = (com.example.movieshow_store.models.Sansion) cachedObject.object;
            cachedObject.minDepth = currentDepth;
        }
        com_example_movieshow_store_models_SansionRealmProxyInterface unmanagedCopy = (com_example_movieshow_store_models_SansionRealmProxyInterface) unmanagedObject;
        com_example_movieshow_store_models_SansionRealmProxyInterface realmSource = (com_example_movieshow_store_models_SansionRealmProxyInterface) realmObject;
        unmanagedCopy.realmSet$id(realmSource.realmGet$id());

        // Deep copy of cliente
        unmanagedCopy.realmSet$cliente(com_example_movieshow_store_models_ClienteRealmProxy.createDetachedCopy(realmSource.realmGet$cliente(), currentDepth + 1, maxDepth, cache));

        // Deep copy of alquiler
        unmanagedCopy.realmSet$alquiler(com_example_movieshow_store_models_AlquilerRealmProxy.createDetachedCopy(realmSource.realmGet$alquiler(), currentDepth + 1, maxDepth, cache));
        unmanagedCopy.realmSet$tipo(realmSource.realmGet$tipo());
        unmanagedCopy.realmSet$dias(realmSource.realmGet$dias());

        return unmanagedObject;
    }

    static com.example.movieshow_store.models.Sansion update(Realm realm, SansionColumnInfo columnInfo, com.example.movieshow_store.models.Sansion realmObject, com.example.movieshow_store.models.Sansion newObject, Map<RealmModel, RealmObjectProxy> cache, Set<ImportFlag> flags) {
        com_example_movieshow_store_models_SansionRealmProxyInterface realmObjectTarget = (com_example_movieshow_store_models_SansionRealmProxyInterface) realmObject;
        com_example_movieshow_store_models_SansionRealmProxyInterface realmObjectSource = (com_example_movieshow_store_models_SansionRealmProxyInterface) newObject;
        Table table = realm.getTable(com.example.movieshow_store.models.Sansion.class);
        OsObjectBuilder builder = new OsObjectBuilder(table, flags);
        builder.addInteger(columnInfo.idColKey, realmObjectSource.realmGet$id());

        com.example.movieshow_store.models.Cliente clienteObj = realmObjectSource.realmGet$cliente();
        if (clienteObj == null) {
            builder.addNull(columnInfo.clienteColKey);
        } else {
            com.example.movieshow_store.models.Cliente cachecliente = (com.example.movieshow_store.models.Cliente) cache.get(clienteObj);
            if (cachecliente != null) {
                builder.addObject(columnInfo.clienteColKey, cachecliente);
            } else {
                builder.addObject(columnInfo.clienteColKey, com_example_movieshow_store_models_ClienteRealmProxy.copyOrUpdate(realm, (com_example_movieshow_store_models_ClienteRealmProxy.ClienteColumnInfo) realm.getSchema().getColumnInfo(com.example.movieshow_store.models.Cliente.class), clienteObj, true, cache, flags));
            }
        }

        com.example.movieshow_store.models.Alquiler alquilerObj = realmObjectSource.realmGet$alquiler();
        if (alquilerObj == null) {
            builder.addNull(columnInfo.alquilerColKey);
        } else {
            com.example.movieshow_store.models.Alquiler cachealquiler = (com.example.movieshow_store.models.Alquiler) cache.get(alquilerObj);
            if (cachealquiler != null) {
                builder.addObject(columnInfo.alquilerColKey, cachealquiler);
            } else {
                builder.addObject(columnInfo.alquilerColKey, com_example_movieshow_store_models_AlquilerRealmProxy.copyOrUpdate(realm, (com_example_movieshow_store_models_AlquilerRealmProxy.AlquilerColumnInfo) realm.getSchema().getColumnInfo(com.example.movieshow_store.models.Alquiler.class), alquilerObj, true, cache, flags));
            }
        }
        builder.addInteger(columnInfo.tipoColKey, realmObjectSource.realmGet$tipo());
        builder.addInteger(columnInfo.diasColKey, realmObjectSource.realmGet$dias());

        builder.updateExistingObject();
        return realmObject;
    }

    @Override
    @SuppressWarnings("ArrayToString")
    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("Sansion = proxy[");
        stringBuilder.append("{id:");
        stringBuilder.append(realmGet$id());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{cliente:");
        stringBuilder.append(realmGet$cliente() != null ? "Cliente" : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{alquiler:");
        stringBuilder.append(realmGet$alquiler() != null ? "Alquiler" : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{tipo:");
        stringBuilder.append(realmGet$tipo());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{dias:");
        stringBuilder.append(realmGet$dias());
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public ProxyState<?> realmGet$proxyState() {
        return proxyState;
    }

    @Override
    public int hashCode() {
        String realmName = proxyState.getRealm$realm().getPath();
        String tableName = proxyState.getRow$realm().getTable().getName();
        long colKey = proxyState.getRow$realm().getObjectKey();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (colKey ^ (colKey >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        com_example_movieshow_store_models_SansionRealmProxy aSansion = (com_example_movieshow_store_models_SansionRealmProxy)o;

        BaseRealm realm = proxyState.getRealm$realm();
        BaseRealm otherRealm = aSansion.proxyState.getRealm$realm();
        String path = realm.getPath();
        String otherPath = otherRealm.getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;
        if (realm.isFrozen() != otherRealm.isFrozen()) return false;
        if (!realm.sharedRealm.getVersionID().equals(otherRealm.sharedRealm.getVersionID())) {
            return false;
        }

        String tableName = proxyState.getRow$realm().getTable().getName();
        String otherTableName = aSansion.proxyState.getRow$realm().getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (proxyState.getRow$realm().getObjectKey() != aSansion.proxyState.getRow$realm().getObjectKey()) return false;

        return true;
    }
}
