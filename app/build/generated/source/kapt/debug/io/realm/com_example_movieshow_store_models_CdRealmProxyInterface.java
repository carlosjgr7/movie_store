package io.realm;


public interface com_example_movieshow_store_models_CdRealmProxyInterface {
    public int realmGet$id();
    public void realmSet$id(int value);
    public String realmGet$condicion();
    public void realmSet$condicion(String value);
    public String realmGet$ubicacion();
    public void realmSet$ubicacion(String value);
    public String realmGet$estado();
    public void realmSet$estado(String value);
}
