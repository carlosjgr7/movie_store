package io.realm;


public interface com_example_movieshow_store_models_ClienteRealmProxyInterface {
    public int realmGet$id();
    public void realmSet$id(int value);
    public String realmGet$name();
    public void realmSet$name(String value);
    public String realmGet$direccion();
    public void realmSet$direccion(String value);
    public String realmGet$telefono();
    public void realmSet$telefono(String value);
    public String realmGet$email();
    public void realmSet$email(String value);
    public String realmGet$dni();
    public void realmSet$dni(String value);
    public java.util.Date realmGet$fecha_nacimiento();
    public void realmSet$fecha_nacimiento(java.util.Date value);
    public java.util.Date realmGet$fecha_inscripcion();
    public void realmSet$fecha_inscripcion(java.util.Date value);
    public String realmGet$tema_interes();
    public void realmSet$tema_interes(String value);
    public String realmGet$estado();
    public void realmSet$estado(String value);
    public String realmGet$usuario();
    public void realmSet$usuario(String value);
    public String realmGet$contrasenna();
    public void realmSet$contrasenna(String value);
}
