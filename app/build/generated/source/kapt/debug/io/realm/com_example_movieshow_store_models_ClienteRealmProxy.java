package io.realm;


import android.annotation.TargetApi;
import android.os.Build;
import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.ImportFlag;
import io.realm.ProxyUtils;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.OsList;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.Property;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Row;
import io.realm.internal.Table;
import io.realm.internal.android.JsonUtils;
import io.realm.internal.objectstore.OsObjectBuilder;
import io.realm.log.RealmLog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressWarnings("all")
public class com_example_movieshow_store_models_ClienteRealmProxy extends com.example.movieshow_store.models.Cliente
    implements RealmObjectProxy, com_example_movieshow_store_models_ClienteRealmProxyInterface {

    static final class ClienteColumnInfo extends ColumnInfo {
        long idColKey;
        long nameColKey;
        long direccionColKey;
        long telefonoColKey;
        long emailColKey;
        long dniColKey;
        long fecha_nacimientoColKey;
        long fecha_inscripcionColKey;
        long tema_interesColKey;
        long estadoColKey;
        long usuarioColKey;
        long contrasennaColKey;

        ClienteColumnInfo(OsSchemaInfo schemaInfo) {
            super(12);
            OsObjectSchemaInfo objectSchemaInfo = schemaInfo.getObjectSchemaInfo("Cliente");
            this.idColKey = addColumnDetails("id", "id", objectSchemaInfo);
            this.nameColKey = addColumnDetails("name", "name", objectSchemaInfo);
            this.direccionColKey = addColumnDetails("direccion", "direccion", objectSchemaInfo);
            this.telefonoColKey = addColumnDetails("telefono", "telefono", objectSchemaInfo);
            this.emailColKey = addColumnDetails("email", "email", objectSchemaInfo);
            this.dniColKey = addColumnDetails("dni", "dni", objectSchemaInfo);
            this.fecha_nacimientoColKey = addColumnDetails("fecha_nacimiento", "fecha_nacimiento", objectSchemaInfo);
            this.fecha_inscripcionColKey = addColumnDetails("fecha_inscripcion", "fecha_inscripcion", objectSchemaInfo);
            this.tema_interesColKey = addColumnDetails("tema_interes", "tema_interes", objectSchemaInfo);
            this.estadoColKey = addColumnDetails("estado", "estado", objectSchemaInfo);
            this.usuarioColKey = addColumnDetails("usuario", "usuario", objectSchemaInfo);
            this.contrasennaColKey = addColumnDetails("contrasenna", "contrasenna", objectSchemaInfo);
        }

        ClienteColumnInfo(ColumnInfo src, boolean mutable) {
            super(src, mutable);
            copy(src, this);
        }

        @Override
        protected final ColumnInfo copy(boolean mutable) {
            return new ClienteColumnInfo(this, mutable);
        }

        @Override
        protected final void copy(ColumnInfo rawSrc, ColumnInfo rawDst) {
            final ClienteColumnInfo src = (ClienteColumnInfo) rawSrc;
            final ClienteColumnInfo dst = (ClienteColumnInfo) rawDst;
            dst.idColKey = src.idColKey;
            dst.nameColKey = src.nameColKey;
            dst.direccionColKey = src.direccionColKey;
            dst.telefonoColKey = src.telefonoColKey;
            dst.emailColKey = src.emailColKey;
            dst.dniColKey = src.dniColKey;
            dst.fecha_nacimientoColKey = src.fecha_nacimientoColKey;
            dst.fecha_inscripcionColKey = src.fecha_inscripcionColKey;
            dst.tema_interesColKey = src.tema_interesColKey;
            dst.estadoColKey = src.estadoColKey;
            dst.usuarioColKey = src.usuarioColKey;
            dst.contrasennaColKey = src.contrasennaColKey;
        }
    }

    private static final OsObjectSchemaInfo expectedObjectSchemaInfo = createExpectedObjectSchemaInfo();

    private ClienteColumnInfo columnInfo;
    private ProxyState<com.example.movieshow_store.models.Cliente> proxyState;

    com_example_movieshow_store_models_ClienteRealmProxy() {
        proxyState.setConstructionFinished();
    }

    @Override
    public void realm$injectObjectContext() {
        if (this.proxyState != null) {
            return;
        }
        final BaseRealm.RealmObjectContext context = BaseRealm.objectContext.get();
        this.columnInfo = (ClienteColumnInfo) context.getColumnInfo();
        this.proxyState = new ProxyState<com.example.movieshow_store.models.Cliente>(this);
        proxyState.setRealm$realm(context.getRealm());
        proxyState.setRow$realm(context.getRow());
        proxyState.setAcceptDefaultValue$realm(context.getAcceptDefaultValue());
        proxyState.setExcludeFields$realm(context.getExcludeFields());
    }

    @Override
    @SuppressWarnings("cast")
    public int realmGet$id() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.idColKey);
    }

    @Override
    public void realmSet$id(int value) {
        if (proxyState.isUnderConstruction()) {
            // default value of the primary key is always ignored.
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        throw new io.realm.exceptions.RealmException("Primary key field 'id' cannot be changed after object was created.");
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$name() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.nameColKey);
    }

    @Override
    public void realmSet$name(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'name' to null.");
            }
            row.getTable().setString(columnInfo.nameColKey, row.getObjectKey(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            throw new IllegalArgumentException("Trying to set non-nullable field 'name' to null.");
        }
        proxyState.getRow$realm().setString(columnInfo.nameColKey, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$direccion() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.direccionColKey);
    }

    @Override
    public void realmSet$direccion(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'direccion' to null.");
            }
            row.getTable().setString(columnInfo.direccionColKey, row.getObjectKey(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            throw new IllegalArgumentException("Trying to set non-nullable field 'direccion' to null.");
        }
        proxyState.getRow$realm().setString(columnInfo.direccionColKey, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$telefono() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.telefonoColKey);
    }

    @Override
    public void realmSet$telefono(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'telefono' to null.");
            }
            row.getTable().setString(columnInfo.telefonoColKey, row.getObjectKey(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            throw new IllegalArgumentException("Trying to set non-nullable field 'telefono' to null.");
        }
        proxyState.getRow$realm().setString(columnInfo.telefonoColKey, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$email() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.emailColKey);
    }

    @Override
    public void realmSet$email(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'email' to null.");
            }
            row.getTable().setString(columnInfo.emailColKey, row.getObjectKey(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            throw new IllegalArgumentException("Trying to set non-nullable field 'email' to null.");
        }
        proxyState.getRow$realm().setString(columnInfo.emailColKey, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$dni() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.dniColKey);
    }

    @Override
    public void realmSet$dni(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'dni' to null.");
            }
            row.getTable().setString(columnInfo.dniColKey, row.getObjectKey(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            throw new IllegalArgumentException("Trying to set non-nullable field 'dni' to null.");
        }
        proxyState.getRow$realm().setString(columnInfo.dniColKey, value);
    }

    @Override
    @SuppressWarnings("cast")
    public Date realmGet$fecha_nacimiento() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.util.Date) proxyState.getRow$realm().getDate(columnInfo.fecha_nacimientoColKey);
    }

    @Override
    public void realmSet$fecha_nacimiento(Date value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'fecha_nacimiento' to null.");
            }
            row.getTable().setDate(columnInfo.fecha_nacimientoColKey, row.getObjectKey(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            throw new IllegalArgumentException("Trying to set non-nullable field 'fecha_nacimiento' to null.");
        }
        proxyState.getRow$realm().setDate(columnInfo.fecha_nacimientoColKey, value);
    }

    @Override
    @SuppressWarnings("cast")
    public Date realmGet$fecha_inscripcion() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.util.Date) proxyState.getRow$realm().getDate(columnInfo.fecha_inscripcionColKey);
    }

    @Override
    public void realmSet$fecha_inscripcion(Date value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'fecha_inscripcion' to null.");
            }
            row.getTable().setDate(columnInfo.fecha_inscripcionColKey, row.getObjectKey(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            throw new IllegalArgumentException("Trying to set non-nullable field 'fecha_inscripcion' to null.");
        }
        proxyState.getRow$realm().setDate(columnInfo.fecha_inscripcionColKey, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$tema_interes() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.tema_interesColKey);
    }

    @Override
    public void realmSet$tema_interes(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'tema_interes' to null.");
            }
            row.getTable().setString(columnInfo.tema_interesColKey, row.getObjectKey(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            throw new IllegalArgumentException("Trying to set non-nullable field 'tema_interes' to null.");
        }
        proxyState.getRow$realm().setString(columnInfo.tema_interesColKey, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$estado() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.estadoColKey);
    }

    @Override
    public void realmSet$estado(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'estado' to null.");
            }
            row.getTable().setString(columnInfo.estadoColKey, row.getObjectKey(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            throw new IllegalArgumentException("Trying to set non-nullable field 'estado' to null.");
        }
        proxyState.getRow$realm().setString(columnInfo.estadoColKey, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$usuario() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.usuarioColKey);
    }

    @Override
    public void realmSet$usuario(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'usuario' to null.");
            }
            row.getTable().setString(columnInfo.usuarioColKey, row.getObjectKey(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            throw new IllegalArgumentException("Trying to set non-nullable field 'usuario' to null.");
        }
        proxyState.getRow$realm().setString(columnInfo.usuarioColKey, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$contrasenna() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.contrasennaColKey);
    }

    @Override
    public void realmSet$contrasenna(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'contrasenna' to null.");
            }
            row.getTable().setString(columnInfo.contrasennaColKey, row.getObjectKey(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            throw new IllegalArgumentException("Trying to set non-nullable field 'contrasenna' to null.");
        }
        proxyState.getRow$realm().setString(columnInfo.contrasennaColKey, value);
    }

    private static OsObjectSchemaInfo createExpectedObjectSchemaInfo() {
        OsObjectSchemaInfo.Builder builder = new OsObjectSchemaInfo.Builder("Cliente", 12, 0);
        builder.addPersistedProperty("id", RealmFieldType.INTEGER, Property.PRIMARY_KEY, Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("name", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("direccion", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("telefono", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("email", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("dni", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("fecha_nacimiento", RealmFieldType.DATE, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("fecha_inscripcion", RealmFieldType.DATE, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("tema_interes", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("estado", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("usuario", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("contrasenna", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        return builder.build();
    }

    public static OsObjectSchemaInfo getExpectedObjectSchemaInfo() {
        return expectedObjectSchemaInfo;
    }

    public static ClienteColumnInfo createColumnInfo(OsSchemaInfo schemaInfo) {
        return new ClienteColumnInfo(schemaInfo);
    }

    public static String getSimpleClassName() {
        return "Cliente";
    }

    public static final class ClassNameHelper {
        public static final String INTERNAL_CLASS_NAME = "Cliente";
    }

    @SuppressWarnings("cast")
    public static com.example.movieshow_store.models.Cliente createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        final List<String> excludeFields = Collections.<String> emptyList();
        com.example.movieshow_store.models.Cliente obj = null;
        if (update) {
            Table table = realm.getTable(com.example.movieshow_store.models.Cliente.class);
            ClienteColumnInfo columnInfo = (ClienteColumnInfo) realm.getSchema().getColumnInfo(com.example.movieshow_store.models.Cliente.class);
            long pkColumnKey = columnInfo.idColKey;
            long colKey = Table.NO_MATCH;
            if (!json.isNull("id")) {
                colKey = table.findFirstLong(pkColumnKey, json.getLong("id"));
            }
            if (colKey != Table.NO_MATCH) {
                final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
                try {
                    objectContext.set(realm, table.getUncheckedRow(colKey), realm.getSchema().getColumnInfo(com.example.movieshow_store.models.Cliente.class), false, Collections.<String> emptyList());
                    obj = new io.realm.com_example_movieshow_store_models_ClienteRealmProxy();
                } finally {
                    objectContext.clear();
                }
            }
        }
        if (obj == null) {
            if (json.has("id")) {
                if (json.isNull("id")) {
                    obj = (io.realm.com_example_movieshow_store_models_ClienteRealmProxy) realm.createObjectInternal(com.example.movieshow_store.models.Cliente.class, null, true, excludeFields);
                } else {
                    obj = (io.realm.com_example_movieshow_store_models_ClienteRealmProxy) realm.createObjectInternal(com.example.movieshow_store.models.Cliente.class, json.getInt("id"), true, excludeFields);
                }
            } else {
                throw new IllegalArgumentException("JSON object doesn't have the primary key field 'id'.");
            }
        }

        final com_example_movieshow_store_models_ClienteRealmProxyInterface objProxy = (com_example_movieshow_store_models_ClienteRealmProxyInterface) obj;
        if (json.has("name")) {
            if (json.isNull("name")) {
                objProxy.realmSet$name(null);
            } else {
                objProxy.realmSet$name((String) json.getString("name"));
            }
        }
        if (json.has("direccion")) {
            if (json.isNull("direccion")) {
                objProxy.realmSet$direccion(null);
            } else {
                objProxy.realmSet$direccion((String) json.getString("direccion"));
            }
        }
        if (json.has("telefono")) {
            if (json.isNull("telefono")) {
                objProxy.realmSet$telefono(null);
            } else {
                objProxy.realmSet$telefono((String) json.getString("telefono"));
            }
        }
        if (json.has("email")) {
            if (json.isNull("email")) {
                objProxy.realmSet$email(null);
            } else {
                objProxy.realmSet$email((String) json.getString("email"));
            }
        }
        if (json.has("dni")) {
            if (json.isNull("dni")) {
                objProxy.realmSet$dni(null);
            } else {
                objProxy.realmSet$dni((String) json.getString("dni"));
            }
        }
        if (json.has("fecha_nacimiento")) {
            if (json.isNull("fecha_nacimiento")) {
                objProxy.realmSet$fecha_nacimiento(null);
            } else {
                Object timestamp = json.get("fecha_nacimiento");
                if (timestamp instanceof String) {
                    objProxy.realmSet$fecha_nacimiento(JsonUtils.stringToDate((String) timestamp));
                } else {
                    objProxy.realmSet$fecha_nacimiento(new Date(json.getLong("fecha_nacimiento")));
                }
            }
        }
        if (json.has("fecha_inscripcion")) {
            if (json.isNull("fecha_inscripcion")) {
                objProxy.realmSet$fecha_inscripcion(null);
            } else {
                Object timestamp = json.get("fecha_inscripcion");
                if (timestamp instanceof String) {
                    objProxy.realmSet$fecha_inscripcion(JsonUtils.stringToDate((String) timestamp));
                } else {
                    objProxy.realmSet$fecha_inscripcion(new Date(json.getLong("fecha_inscripcion")));
                }
            }
        }
        if (json.has("tema_interes")) {
            if (json.isNull("tema_interes")) {
                objProxy.realmSet$tema_interes(null);
            } else {
                objProxy.realmSet$tema_interes((String) json.getString("tema_interes"));
            }
        }
        if (json.has("estado")) {
            if (json.isNull("estado")) {
                objProxy.realmSet$estado(null);
            } else {
                objProxy.realmSet$estado((String) json.getString("estado"));
            }
        }
        if (json.has("usuario")) {
            if (json.isNull("usuario")) {
                objProxy.realmSet$usuario(null);
            } else {
                objProxy.realmSet$usuario((String) json.getString("usuario"));
            }
        }
        if (json.has("contrasenna")) {
            if (json.isNull("contrasenna")) {
                objProxy.realmSet$contrasenna(null);
            } else {
                objProxy.realmSet$contrasenna((String) json.getString("contrasenna"));
            }
        }
        return obj;
    }

    @SuppressWarnings("cast")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static com.example.movieshow_store.models.Cliente createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        boolean jsonHasPrimaryKey = false;
        final com.example.movieshow_store.models.Cliente obj = new com.example.movieshow_store.models.Cliente();
        final com_example_movieshow_store_models_ClienteRealmProxyInterface objProxy = (com_example_movieshow_store_models_ClienteRealmProxyInterface) obj;
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (false) {
            } else if (name.equals("id")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$id((int) reader.nextInt());
                } else {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'id' to null.");
                }
                jsonHasPrimaryKey = true;
            } else if (name.equals("name")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$name((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$name(null);
                }
            } else if (name.equals("direccion")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$direccion((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$direccion(null);
                }
            } else if (name.equals("telefono")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$telefono((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$telefono(null);
                }
            } else if (name.equals("email")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$email((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$email(null);
                }
            } else if (name.equals("dni")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$dni((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$dni(null);
                }
            } else if (name.equals("fecha_nacimiento")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    objProxy.realmSet$fecha_nacimiento(null);
                } else if (reader.peek() == JsonToken.NUMBER) {
                    long timestamp = reader.nextLong();
                    if (timestamp > -1) {
                        objProxy.realmSet$fecha_nacimiento(new Date(timestamp));
                    }
                } else {
                    objProxy.realmSet$fecha_nacimiento(JsonUtils.stringToDate(reader.nextString()));
                }
            } else if (name.equals("fecha_inscripcion")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    objProxy.realmSet$fecha_inscripcion(null);
                } else if (reader.peek() == JsonToken.NUMBER) {
                    long timestamp = reader.nextLong();
                    if (timestamp > -1) {
                        objProxy.realmSet$fecha_inscripcion(new Date(timestamp));
                    }
                } else {
                    objProxy.realmSet$fecha_inscripcion(JsonUtils.stringToDate(reader.nextString()));
                }
            } else if (name.equals("tema_interes")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$tema_interes((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$tema_interes(null);
                }
            } else if (name.equals("estado")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$estado((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$estado(null);
                }
            } else if (name.equals("usuario")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$usuario((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$usuario(null);
                }
            } else if (name.equals("contrasenna")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$contrasenna((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$contrasenna(null);
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        if (!jsonHasPrimaryKey) {
            throw new IllegalArgumentException("JSON object doesn't have the primary key field 'id'.");
        }
        return realm.copyToRealm(obj);
    }

    private static com_example_movieshow_store_models_ClienteRealmProxy newProxyInstance(BaseRealm realm, Row row) {
        // Ignore default values to avoid creating unexpected objects from RealmModel/RealmList fields
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        objectContext.set(realm, row, realm.getSchema().getColumnInfo(com.example.movieshow_store.models.Cliente.class), false, Collections.<String>emptyList());
        io.realm.com_example_movieshow_store_models_ClienteRealmProxy obj = new io.realm.com_example_movieshow_store_models_ClienteRealmProxy();
        objectContext.clear();
        return obj;
    }

    public static com.example.movieshow_store.models.Cliente copyOrUpdate(Realm realm, ClienteColumnInfo columnInfo, com.example.movieshow_store.models.Cliente object, boolean update, Map<RealmModel,RealmObjectProxy> cache, Set<ImportFlag> flags) {
        if (object instanceof RealmObjectProxy && !RealmObject.isFrozen(object) && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null) {
            final BaseRealm otherRealm = ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm();
            if (otherRealm.threadId != realm.threadId) {
                throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
            }
            if (otherRealm.getPath().equals(realm.getPath())) {
                return object;
            }
        }
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        RealmObjectProxy cachedRealmObject = cache.get(object);
        if (cachedRealmObject != null) {
            return (com.example.movieshow_store.models.Cliente) cachedRealmObject;
        }

        com.example.movieshow_store.models.Cliente realmObject = null;
        boolean canUpdate = update;
        if (canUpdate) {
            Table table = realm.getTable(com.example.movieshow_store.models.Cliente.class);
            long pkColumnKey = columnInfo.idColKey;
            long colKey = table.findFirstLong(pkColumnKey, ((com_example_movieshow_store_models_ClienteRealmProxyInterface) object).realmGet$id());
            if (colKey == Table.NO_MATCH) {
                canUpdate = false;
            } else {
                try {
                    objectContext.set(realm, table.getUncheckedRow(colKey), columnInfo, false, Collections.<String> emptyList());
                    realmObject = new io.realm.com_example_movieshow_store_models_ClienteRealmProxy();
                    cache.put(object, (RealmObjectProxy) realmObject);
                } finally {
                    objectContext.clear();
                }
            }
        }

        return (canUpdate) ? update(realm, columnInfo, realmObject, object, cache, flags) : copy(realm, columnInfo, object, update, cache, flags);
    }

    public static com.example.movieshow_store.models.Cliente copy(Realm realm, ClienteColumnInfo columnInfo, com.example.movieshow_store.models.Cliente newObject, boolean update, Map<RealmModel,RealmObjectProxy> cache, Set<ImportFlag> flags) {
        RealmObjectProxy cachedRealmObject = cache.get(newObject);
        if (cachedRealmObject != null) {
            return (com.example.movieshow_store.models.Cliente) cachedRealmObject;
        }

        com_example_movieshow_store_models_ClienteRealmProxyInterface realmObjectSource = (com_example_movieshow_store_models_ClienteRealmProxyInterface) newObject;

        Table table = realm.getTable(com.example.movieshow_store.models.Cliente.class);
        OsObjectBuilder builder = new OsObjectBuilder(table, flags);

        // Add all non-"object reference" fields
        builder.addInteger(columnInfo.idColKey, realmObjectSource.realmGet$id());
        builder.addString(columnInfo.nameColKey, realmObjectSource.realmGet$name());
        builder.addString(columnInfo.direccionColKey, realmObjectSource.realmGet$direccion());
        builder.addString(columnInfo.telefonoColKey, realmObjectSource.realmGet$telefono());
        builder.addString(columnInfo.emailColKey, realmObjectSource.realmGet$email());
        builder.addString(columnInfo.dniColKey, realmObjectSource.realmGet$dni());
        builder.addDate(columnInfo.fecha_nacimientoColKey, realmObjectSource.realmGet$fecha_nacimiento());
        builder.addDate(columnInfo.fecha_inscripcionColKey, realmObjectSource.realmGet$fecha_inscripcion());
        builder.addString(columnInfo.tema_interesColKey, realmObjectSource.realmGet$tema_interes());
        builder.addString(columnInfo.estadoColKey, realmObjectSource.realmGet$estado());
        builder.addString(columnInfo.usuarioColKey, realmObjectSource.realmGet$usuario());
        builder.addString(columnInfo.contrasennaColKey, realmObjectSource.realmGet$contrasenna());

        // Create the underlying object and cache it before setting any object/objectlist references
        // This will allow us to break any circular dependencies by using the object cache.
        Row row = builder.createNewObject();
        io.realm.com_example_movieshow_store_models_ClienteRealmProxy realmObjectCopy = newProxyInstance(realm, row);
        cache.put(newObject, realmObjectCopy);

        return realmObjectCopy;
    }

    public static long insert(Realm realm, com.example.movieshow_store.models.Cliente object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && !RealmObject.isFrozen(object) && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getObjectKey();
        }
        Table table = realm.getTable(com.example.movieshow_store.models.Cliente.class);
        long tableNativePtr = table.getNativePtr();
        ClienteColumnInfo columnInfo = (ClienteColumnInfo) realm.getSchema().getColumnInfo(com.example.movieshow_store.models.Cliente.class);
        long pkColumnKey = columnInfo.idColKey;
        long colKey = Table.NO_MATCH;
        Object primaryKeyValue = ((com_example_movieshow_store_models_ClienteRealmProxyInterface) object).realmGet$id();
        if (primaryKeyValue != null) {
            colKey = Table.nativeFindFirstInt(tableNativePtr, pkColumnKey, ((com_example_movieshow_store_models_ClienteRealmProxyInterface) object).realmGet$id());
        }
        if (colKey == Table.NO_MATCH) {
            colKey = OsObject.createRowWithPrimaryKey(table, pkColumnKey, ((com_example_movieshow_store_models_ClienteRealmProxyInterface) object).realmGet$id());
        } else {
            Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
        }
        cache.put(object, colKey);
        String realmGet$name = ((com_example_movieshow_store_models_ClienteRealmProxyInterface) object).realmGet$name();
        if (realmGet$name != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.nameColKey, colKey, realmGet$name, false);
        }
        String realmGet$direccion = ((com_example_movieshow_store_models_ClienteRealmProxyInterface) object).realmGet$direccion();
        if (realmGet$direccion != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.direccionColKey, colKey, realmGet$direccion, false);
        }
        String realmGet$telefono = ((com_example_movieshow_store_models_ClienteRealmProxyInterface) object).realmGet$telefono();
        if (realmGet$telefono != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.telefonoColKey, colKey, realmGet$telefono, false);
        }
        String realmGet$email = ((com_example_movieshow_store_models_ClienteRealmProxyInterface) object).realmGet$email();
        if (realmGet$email != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.emailColKey, colKey, realmGet$email, false);
        }
        String realmGet$dni = ((com_example_movieshow_store_models_ClienteRealmProxyInterface) object).realmGet$dni();
        if (realmGet$dni != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.dniColKey, colKey, realmGet$dni, false);
        }
        java.util.Date realmGet$fecha_nacimiento = ((com_example_movieshow_store_models_ClienteRealmProxyInterface) object).realmGet$fecha_nacimiento();
        if (realmGet$fecha_nacimiento != null) {
            Table.nativeSetTimestamp(tableNativePtr, columnInfo.fecha_nacimientoColKey, colKey, realmGet$fecha_nacimiento.getTime(), false);
        }
        java.util.Date realmGet$fecha_inscripcion = ((com_example_movieshow_store_models_ClienteRealmProxyInterface) object).realmGet$fecha_inscripcion();
        if (realmGet$fecha_inscripcion != null) {
            Table.nativeSetTimestamp(tableNativePtr, columnInfo.fecha_inscripcionColKey, colKey, realmGet$fecha_inscripcion.getTime(), false);
        }
        String realmGet$tema_interes = ((com_example_movieshow_store_models_ClienteRealmProxyInterface) object).realmGet$tema_interes();
        if (realmGet$tema_interes != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.tema_interesColKey, colKey, realmGet$tema_interes, false);
        }
        String realmGet$estado = ((com_example_movieshow_store_models_ClienteRealmProxyInterface) object).realmGet$estado();
        if (realmGet$estado != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.estadoColKey, colKey, realmGet$estado, false);
        }
        String realmGet$usuario = ((com_example_movieshow_store_models_ClienteRealmProxyInterface) object).realmGet$usuario();
        if (realmGet$usuario != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.usuarioColKey, colKey, realmGet$usuario, false);
        }
        String realmGet$contrasenna = ((com_example_movieshow_store_models_ClienteRealmProxyInterface) object).realmGet$contrasenna();
        if (realmGet$contrasenna != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.contrasennaColKey, colKey, realmGet$contrasenna, false);
        }
        return colKey;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.example.movieshow_store.models.Cliente.class);
        long tableNativePtr = table.getNativePtr();
        ClienteColumnInfo columnInfo = (ClienteColumnInfo) realm.getSchema().getColumnInfo(com.example.movieshow_store.models.Cliente.class);
        long pkColumnKey = columnInfo.idColKey;
        com.example.movieshow_store.models.Cliente object = null;
        while (objects.hasNext()) {
            object = (com.example.movieshow_store.models.Cliente) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && !RealmObject.isFrozen(object) && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getObjectKey());
                continue;
            }
            long colKey = Table.NO_MATCH;
            Object primaryKeyValue = ((com_example_movieshow_store_models_ClienteRealmProxyInterface) object).realmGet$id();
            if (primaryKeyValue != null) {
                colKey = Table.nativeFindFirstInt(tableNativePtr, pkColumnKey, ((com_example_movieshow_store_models_ClienteRealmProxyInterface) object).realmGet$id());
            }
            if (colKey == Table.NO_MATCH) {
                colKey = OsObject.createRowWithPrimaryKey(table, pkColumnKey, ((com_example_movieshow_store_models_ClienteRealmProxyInterface) object).realmGet$id());
            } else {
                Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
            }
            cache.put(object, colKey);
            String realmGet$name = ((com_example_movieshow_store_models_ClienteRealmProxyInterface) object).realmGet$name();
            if (realmGet$name != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.nameColKey, colKey, realmGet$name, false);
            }
            String realmGet$direccion = ((com_example_movieshow_store_models_ClienteRealmProxyInterface) object).realmGet$direccion();
            if (realmGet$direccion != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.direccionColKey, colKey, realmGet$direccion, false);
            }
            String realmGet$telefono = ((com_example_movieshow_store_models_ClienteRealmProxyInterface) object).realmGet$telefono();
            if (realmGet$telefono != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.telefonoColKey, colKey, realmGet$telefono, false);
            }
            String realmGet$email = ((com_example_movieshow_store_models_ClienteRealmProxyInterface) object).realmGet$email();
            if (realmGet$email != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.emailColKey, colKey, realmGet$email, false);
            }
            String realmGet$dni = ((com_example_movieshow_store_models_ClienteRealmProxyInterface) object).realmGet$dni();
            if (realmGet$dni != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.dniColKey, colKey, realmGet$dni, false);
            }
            java.util.Date realmGet$fecha_nacimiento = ((com_example_movieshow_store_models_ClienteRealmProxyInterface) object).realmGet$fecha_nacimiento();
            if (realmGet$fecha_nacimiento != null) {
                Table.nativeSetTimestamp(tableNativePtr, columnInfo.fecha_nacimientoColKey, colKey, realmGet$fecha_nacimiento.getTime(), false);
            }
            java.util.Date realmGet$fecha_inscripcion = ((com_example_movieshow_store_models_ClienteRealmProxyInterface) object).realmGet$fecha_inscripcion();
            if (realmGet$fecha_inscripcion != null) {
                Table.nativeSetTimestamp(tableNativePtr, columnInfo.fecha_inscripcionColKey, colKey, realmGet$fecha_inscripcion.getTime(), false);
            }
            String realmGet$tema_interes = ((com_example_movieshow_store_models_ClienteRealmProxyInterface) object).realmGet$tema_interes();
            if (realmGet$tema_interes != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.tema_interesColKey, colKey, realmGet$tema_interes, false);
            }
            String realmGet$estado = ((com_example_movieshow_store_models_ClienteRealmProxyInterface) object).realmGet$estado();
            if (realmGet$estado != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.estadoColKey, colKey, realmGet$estado, false);
            }
            String realmGet$usuario = ((com_example_movieshow_store_models_ClienteRealmProxyInterface) object).realmGet$usuario();
            if (realmGet$usuario != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.usuarioColKey, colKey, realmGet$usuario, false);
            }
            String realmGet$contrasenna = ((com_example_movieshow_store_models_ClienteRealmProxyInterface) object).realmGet$contrasenna();
            if (realmGet$contrasenna != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.contrasennaColKey, colKey, realmGet$contrasenna, false);
            }
        }
    }

    public static long insertOrUpdate(Realm realm, com.example.movieshow_store.models.Cliente object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && !RealmObject.isFrozen(object) && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getObjectKey();
        }
        Table table = realm.getTable(com.example.movieshow_store.models.Cliente.class);
        long tableNativePtr = table.getNativePtr();
        ClienteColumnInfo columnInfo = (ClienteColumnInfo) realm.getSchema().getColumnInfo(com.example.movieshow_store.models.Cliente.class);
        long pkColumnKey = columnInfo.idColKey;
        long colKey = Table.NO_MATCH;
        Object primaryKeyValue = ((com_example_movieshow_store_models_ClienteRealmProxyInterface) object).realmGet$id();
        if (primaryKeyValue != null) {
            colKey = Table.nativeFindFirstInt(tableNativePtr, pkColumnKey, ((com_example_movieshow_store_models_ClienteRealmProxyInterface) object).realmGet$id());
        }
        if (colKey == Table.NO_MATCH) {
            colKey = OsObject.createRowWithPrimaryKey(table, pkColumnKey, ((com_example_movieshow_store_models_ClienteRealmProxyInterface) object).realmGet$id());
        }
        cache.put(object, colKey);
        String realmGet$name = ((com_example_movieshow_store_models_ClienteRealmProxyInterface) object).realmGet$name();
        if (realmGet$name != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.nameColKey, colKey, realmGet$name, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.nameColKey, colKey, false);
        }
        String realmGet$direccion = ((com_example_movieshow_store_models_ClienteRealmProxyInterface) object).realmGet$direccion();
        if (realmGet$direccion != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.direccionColKey, colKey, realmGet$direccion, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.direccionColKey, colKey, false);
        }
        String realmGet$telefono = ((com_example_movieshow_store_models_ClienteRealmProxyInterface) object).realmGet$telefono();
        if (realmGet$telefono != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.telefonoColKey, colKey, realmGet$telefono, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.telefonoColKey, colKey, false);
        }
        String realmGet$email = ((com_example_movieshow_store_models_ClienteRealmProxyInterface) object).realmGet$email();
        if (realmGet$email != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.emailColKey, colKey, realmGet$email, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.emailColKey, colKey, false);
        }
        String realmGet$dni = ((com_example_movieshow_store_models_ClienteRealmProxyInterface) object).realmGet$dni();
        if (realmGet$dni != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.dniColKey, colKey, realmGet$dni, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.dniColKey, colKey, false);
        }
        java.util.Date realmGet$fecha_nacimiento = ((com_example_movieshow_store_models_ClienteRealmProxyInterface) object).realmGet$fecha_nacimiento();
        if (realmGet$fecha_nacimiento != null) {
            Table.nativeSetTimestamp(tableNativePtr, columnInfo.fecha_nacimientoColKey, colKey, realmGet$fecha_nacimiento.getTime(), false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.fecha_nacimientoColKey, colKey, false);
        }
        java.util.Date realmGet$fecha_inscripcion = ((com_example_movieshow_store_models_ClienteRealmProxyInterface) object).realmGet$fecha_inscripcion();
        if (realmGet$fecha_inscripcion != null) {
            Table.nativeSetTimestamp(tableNativePtr, columnInfo.fecha_inscripcionColKey, colKey, realmGet$fecha_inscripcion.getTime(), false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.fecha_inscripcionColKey, colKey, false);
        }
        String realmGet$tema_interes = ((com_example_movieshow_store_models_ClienteRealmProxyInterface) object).realmGet$tema_interes();
        if (realmGet$tema_interes != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.tema_interesColKey, colKey, realmGet$tema_interes, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.tema_interesColKey, colKey, false);
        }
        String realmGet$estado = ((com_example_movieshow_store_models_ClienteRealmProxyInterface) object).realmGet$estado();
        if (realmGet$estado != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.estadoColKey, colKey, realmGet$estado, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.estadoColKey, colKey, false);
        }
        String realmGet$usuario = ((com_example_movieshow_store_models_ClienteRealmProxyInterface) object).realmGet$usuario();
        if (realmGet$usuario != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.usuarioColKey, colKey, realmGet$usuario, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.usuarioColKey, colKey, false);
        }
        String realmGet$contrasenna = ((com_example_movieshow_store_models_ClienteRealmProxyInterface) object).realmGet$contrasenna();
        if (realmGet$contrasenna != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.contrasennaColKey, colKey, realmGet$contrasenna, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.contrasennaColKey, colKey, false);
        }
        return colKey;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.example.movieshow_store.models.Cliente.class);
        long tableNativePtr = table.getNativePtr();
        ClienteColumnInfo columnInfo = (ClienteColumnInfo) realm.getSchema().getColumnInfo(com.example.movieshow_store.models.Cliente.class);
        long pkColumnKey = columnInfo.idColKey;
        com.example.movieshow_store.models.Cliente object = null;
        while (objects.hasNext()) {
            object = (com.example.movieshow_store.models.Cliente) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && !RealmObject.isFrozen(object) && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getObjectKey());
                continue;
            }
            long colKey = Table.NO_MATCH;
            Object primaryKeyValue = ((com_example_movieshow_store_models_ClienteRealmProxyInterface) object).realmGet$id();
            if (primaryKeyValue != null) {
                colKey = Table.nativeFindFirstInt(tableNativePtr, pkColumnKey, ((com_example_movieshow_store_models_ClienteRealmProxyInterface) object).realmGet$id());
            }
            if (colKey == Table.NO_MATCH) {
                colKey = OsObject.createRowWithPrimaryKey(table, pkColumnKey, ((com_example_movieshow_store_models_ClienteRealmProxyInterface) object).realmGet$id());
            }
            cache.put(object, colKey);
            String realmGet$name = ((com_example_movieshow_store_models_ClienteRealmProxyInterface) object).realmGet$name();
            if (realmGet$name != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.nameColKey, colKey, realmGet$name, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.nameColKey, colKey, false);
            }
            String realmGet$direccion = ((com_example_movieshow_store_models_ClienteRealmProxyInterface) object).realmGet$direccion();
            if (realmGet$direccion != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.direccionColKey, colKey, realmGet$direccion, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.direccionColKey, colKey, false);
            }
            String realmGet$telefono = ((com_example_movieshow_store_models_ClienteRealmProxyInterface) object).realmGet$telefono();
            if (realmGet$telefono != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.telefonoColKey, colKey, realmGet$telefono, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.telefonoColKey, colKey, false);
            }
            String realmGet$email = ((com_example_movieshow_store_models_ClienteRealmProxyInterface) object).realmGet$email();
            if (realmGet$email != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.emailColKey, colKey, realmGet$email, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.emailColKey, colKey, false);
            }
            String realmGet$dni = ((com_example_movieshow_store_models_ClienteRealmProxyInterface) object).realmGet$dni();
            if (realmGet$dni != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.dniColKey, colKey, realmGet$dni, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.dniColKey, colKey, false);
            }
            java.util.Date realmGet$fecha_nacimiento = ((com_example_movieshow_store_models_ClienteRealmProxyInterface) object).realmGet$fecha_nacimiento();
            if (realmGet$fecha_nacimiento != null) {
                Table.nativeSetTimestamp(tableNativePtr, columnInfo.fecha_nacimientoColKey, colKey, realmGet$fecha_nacimiento.getTime(), false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.fecha_nacimientoColKey, colKey, false);
            }
            java.util.Date realmGet$fecha_inscripcion = ((com_example_movieshow_store_models_ClienteRealmProxyInterface) object).realmGet$fecha_inscripcion();
            if (realmGet$fecha_inscripcion != null) {
                Table.nativeSetTimestamp(tableNativePtr, columnInfo.fecha_inscripcionColKey, colKey, realmGet$fecha_inscripcion.getTime(), false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.fecha_inscripcionColKey, colKey, false);
            }
            String realmGet$tema_interes = ((com_example_movieshow_store_models_ClienteRealmProxyInterface) object).realmGet$tema_interes();
            if (realmGet$tema_interes != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.tema_interesColKey, colKey, realmGet$tema_interes, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.tema_interesColKey, colKey, false);
            }
            String realmGet$estado = ((com_example_movieshow_store_models_ClienteRealmProxyInterface) object).realmGet$estado();
            if (realmGet$estado != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.estadoColKey, colKey, realmGet$estado, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.estadoColKey, colKey, false);
            }
            String realmGet$usuario = ((com_example_movieshow_store_models_ClienteRealmProxyInterface) object).realmGet$usuario();
            if (realmGet$usuario != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.usuarioColKey, colKey, realmGet$usuario, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.usuarioColKey, colKey, false);
            }
            String realmGet$contrasenna = ((com_example_movieshow_store_models_ClienteRealmProxyInterface) object).realmGet$contrasenna();
            if (realmGet$contrasenna != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.contrasennaColKey, colKey, realmGet$contrasenna, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.contrasennaColKey, colKey, false);
            }
        }
    }

    public static com.example.movieshow_store.models.Cliente createDetachedCopy(com.example.movieshow_store.models.Cliente realmObject, int currentDepth, int maxDepth, Map<RealmModel, CacheData<RealmModel>> cache) {
        if (currentDepth > maxDepth || realmObject == null) {
            return null;
        }
        CacheData<RealmModel> cachedObject = cache.get(realmObject);
        com.example.movieshow_store.models.Cliente unmanagedObject;
        if (cachedObject == null) {
            unmanagedObject = new com.example.movieshow_store.models.Cliente();
            cache.put(realmObject, new RealmObjectProxy.CacheData<RealmModel>(currentDepth, unmanagedObject));
        } else {
            // Reuse cached object or recreate it because it was encountered at a lower depth.
            if (currentDepth >= cachedObject.minDepth) {
                return (com.example.movieshow_store.models.Cliente) cachedObject.object;
            }
            unmanagedObject = (com.example.movieshow_store.models.Cliente) cachedObject.object;
            cachedObject.minDepth = currentDepth;
        }
        com_example_movieshow_store_models_ClienteRealmProxyInterface unmanagedCopy = (com_example_movieshow_store_models_ClienteRealmProxyInterface) unmanagedObject;
        com_example_movieshow_store_models_ClienteRealmProxyInterface realmSource = (com_example_movieshow_store_models_ClienteRealmProxyInterface) realmObject;
        unmanagedCopy.realmSet$id(realmSource.realmGet$id());
        unmanagedCopy.realmSet$name(realmSource.realmGet$name());
        unmanagedCopy.realmSet$direccion(realmSource.realmGet$direccion());
        unmanagedCopy.realmSet$telefono(realmSource.realmGet$telefono());
        unmanagedCopy.realmSet$email(realmSource.realmGet$email());
        unmanagedCopy.realmSet$dni(realmSource.realmGet$dni());
        unmanagedCopy.realmSet$fecha_nacimiento(realmSource.realmGet$fecha_nacimiento());
        unmanagedCopy.realmSet$fecha_inscripcion(realmSource.realmGet$fecha_inscripcion());
        unmanagedCopy.realmSet$tema_interes(realmSource.realmGet$tema_interes());
        unmanagedCopy.realmSet$estado(realmSource.realmGet$estado());
        unmanagedCopy.realmSet$usuario(realmSource.realmGet$usuario());
        unmanagedCopy.realmSet$contrasenna(realmSource.realmGet$contrasenna());

        return unmanagedObject;
    }

    static com.example.movieshow_store.models.Cliente update(Realm realm, ClienteColumnInfo columnInfo, com.example.movieshow_store.models.Cliente realmObject, com.example.movieshow_store.models.Cliente newObject, Map<RealmModel, RealmObjectProxy> cache, Set<ImportFlag> flags) {
        com_example_movieshow_store_models_ClienteRealmProxyInterface realmObjectTarget = (com_example_movieshow_store_models_ClienteRealmProxyInterface) realmObject;
        com_example_movieshow_store_models_ClienteRealmProxyInterface realmObjectSource = (com_example_movieshow_store_models_ClienteRealmProxyInterface) newObject;
        Table table = realm.getTable(com.example.movieshow_store.models.Cliente.class);
        OsObjectBuilder builder = new OsObjectBuilder(table, flags);
        builder.addInteger(columnInfo.idColKey, realmObjectSource.realmGet$id());
        builder.addString(columnInfo.nameColKey, realmObjectSource.realmGet$name());
        builder.addString(columnInfo.direccionColKey, realmObjectSource.realmGet$direccion());
        builder.addString(columnInfo.telefonoColKey, realmObjectSource.realmGet$telefono());
        builder.addString(columnInfo.emailColKey, realmObjectSource.realmGet$email());
        builder.addString(columnInfo.dniColKey, realmObjectSource.realmGet$dni());
        builder.addDate(columnInfo.fecha_nacimientoColKey, realmObjectSource.realmGet$fecha_nacimiento());
        builder.addDate(columnInfo.fecha_inscripcionColKey, realmObjectSource.realmGet$fecha_inscripcion());
        builder.addString(columnInfo.tema_interesColKey, realmObjectSource.realmGet$tema_interes());
        builder.addString(columnInfo.estadoColKey, realmObjectSource.realmGet$estado());
        builder.addString(columnInfo.usuarioColKey, realmObjectSource.realmGet$usuario());
        builder.addString(columnInfo.contrasennaColKey, realmObjectSource.realmGet$contrasenna());

        builder.updateExistingObject();
        return realmObject;
    }

    @Override
    @SuppressWarnings("ArrayToString")
    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("Cliente = proxy[");
        stringBuilder.append("{id:");
        stringBuilder.append(realmGet$id());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{name:");
        stringBuilder.append(realmGet$name());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{direccion:");
        stringBuilder.append(realmGet$direccion());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{telefono:");
        stringBuilder.append(realmGet$telefono());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{email:");
        stringBuilder.append(realmGet$email());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{dni:");
        stringBuilder.append(realmGet$dni());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{fecha_nacimiento:");
        stringBuilder.append(realmGet$fecha_nacimiento());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{fecha_inscripcion:");
        stringBuilder.append(realmGet$fecha_inscripcion());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{tema_interes:");
        stringBuilder.append(realmGet$tema_interes());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{estado:");
        stringBuilder.append(realmGet$estado());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{usuario:");
        stringBuilder.append(realmGet$usuario());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{contrasenna:");
        stringBuilder.append(realmGet$contrasenna());
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public ProxyState<?> realmGet$proxyState() {
        return proxyState;
    }

    @Override
    public int hashCode() {
        String realmName = proxyState.getRealm$realm().getPath();
        String tableName = proxyState.getRow$realm().getTable().getName();
        long colKey = proxyState.getRow$realm().getObjectKey();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (colKey ^ (colKey >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        com_example_movieshow_store_models_ClienteRealmProxy aCliente = (com_example_movieshow_store_models_ClienteRealmProxy)o;

        BaseRealm realm = proxyState.getRealm$realm();
        BaseRealm otherRealm = aCliente.proxyState.getRealm$realm();
        String path = realm.getPath();
        String otherPath = otherRealm.getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;
        if (realm.isFrozen() != otherRealm.isFrozen()) return false;
        if (!realm.sharedRealm.getVersionID().equals(otherRealm.sharedRealm.getVersionID())) {
            return false;
        }

        String tableName = proxyState.getRow$realm().getTable().getName();
        String otherTableName = aCliente.proxyState.getRow$realm().getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (proxyState.getRow$realm().getObjectKey() != aCliente.proxyState.getRow$realm().getObjectKey()) return false;

        return true;
    }
}
