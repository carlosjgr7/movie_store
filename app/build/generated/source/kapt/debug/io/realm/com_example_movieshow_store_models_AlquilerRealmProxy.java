package io.realm;


import android.annotation.TargetApi;
import android.os.Build;
import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.ImportFlag;
import io.realm.ProxyUtils;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.OsList;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.Property;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Row;
import io.realm.internal.Table;
import io.realm.internal.android.JsonUtils;
import io.realm.internal.objectstore.OsObjectBuilder;
import io.realm.log.RealmLog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressWarnings("all")
public class com_example_movieshow_store_models_AlquilerRealmProxy extends com.example.movieshow_store.models.Alquiler
    implements RealmObjectProxy, com_example_movieshow_store_models_AlquilerRealmProxyInterface {

    static final class AlquilerColumnInfo extends ColumnInfo {
        long idColKey;
        long clienteColKey;
        long cdColKey;
        long fecha_prestamoColKey;
        long precioColKey;
        long diasColKey;
        long fecha_devolucionColKey;

        AlquilerColumnInfo(OsSchemaInfo schemaInfo) {
            super(7);
            OsObjectSchemaInfo objectSchemaInfo = schemaInfo.getObjectSchemaInfo("Alquiler");
            this.idColKey = addColumnDetails("id", "id", objectSchemaInfo);
            this.clienteColKey = addColumnDetails("cliente", "cliente", objectSchemaInfo);
            this.cdColKey = addColumnDetails("cd", "cd", objectSchemaInfo);
            this.fecha_prestamoColKey = addColumnDetails("fecha_prestamo", "fecha_prestamo", objectSchemaInfo);
            this.precioColKey = addColumnDetails("precio", "precio", objectSchemaInfo);
            this.diasColKey = addColumnDetails("dias", "dias", objectSchemaInfo);
            this.fecha_devolucionColKey = addColumnDetails("fecha_devolucion", "fecha_devolucion", objectSchemaInfo);
        }

        AlquilerColumnInfo(ColumnInfo src, boolean mutable) {
            super(src, mutable);
            copy(src, this);
        }

        @Override
        protected final ColumnInfo copy(boolean mutable) {
            return new AlquilerColumnInfo(this, mutable);
        }

        @Override
        protected final void copy(ColumnInfo rawSrc, ColumnInfo rawDst) {
            final AlquilerColumnInfo src = (AlquilerColumnInfo) rawSrc;
            final AlquilerColumnInfo dst = (AlquilerColumnInfo) rawDst;
            dst.idColKey = src.idColKey;
            dst.clienteColKey = src.clienteColKey;
            dst.cdColKey = src.cdColKey;
            dst.fecha_prestamoColKey = src.fecha_prestamoColKey;
            dst.precioColKey = src.precioColKey;
            dst.diasColKey = src.diasColKey;
            dst.fecha_devolucionColKey = src.fecha_devolucionColKey;
        }
    }

    private static final OsObjectSchemaInfo expectedObjectSchemaInfo = createExpectedObjectSchemaInfo();

    private AlquilerColumnInfo columnInfo;
    private ProxyState<com.example.movieshow_store.models.Alquiler> proxyState;

    com_example_movieshow_store_models_AlquilerRealmProxy() {
        proxyState.setConstructionFinished();
    }

    @Override
    public void realm$injectObjectContext() {
        if (this.proxyState != null) {
            return;
        }
        final BaseRealm.RealmObjectContext context = BaseRealm.objectContext.get();
        this.columnInfo = (AlquilerColumnInfo) context.getColumnInfo();
        this.proxyState = new ProxyState<com.example.movieshow_store.models.Alquiler>(this);
        proxyState.setRealm$realm(context.getRealm());
        proxyState.setRow$realm(context.getRow());
        proxyState.setAcceptDefaultValue$realm(context.getAcceptDefaultValue());
        proxyState.setExcludeFields$realm(context.getExcludeFields());
    }

    @Override
    @SuppressWarnings("cast")
    public int realmGet$id() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.idColKey);
    }

    @Override
    public void realmSet$id(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.idColKey, row.getObjectKey(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.idColKey, value);
    }

    @Override
    public com.example.movieshow_store.models.Cliente realmGet$cliente() {
        proxyState.getRealm$realm().checkIfValid();
        if (proxyState.getRow$realm().isNullLink(columnInfo.clienteColKey)) {
            return null;
        }
        return proxyState.getRealm$realm().get(com.example.movieshow_store.models.Cliente.class, proxyState.getRow$realm().getLink(columnInfo.clienteColKey), false, Collections.<String>emptyList());
    }

    @Override
    public void realmSet$cliente(com.example.movieshow_store.models.Cliente value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            if (proxyState.getExcludeFields$realm().contains("cliente")) {
                return;
            }
            if (value != null && !RealmObject.isManaged(value)) {
                value = ((Realm) proxyState.getRealm$realm()).copyToRealm(value);
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                // Table#nullifyLink() does not support default value. Just using Row.
                row.nullifyLink(columnInfo.clienteColKey);
                return;
            }
            proxyState.checkValidObject(value);
            row.getTable().setLink(columnInfo.clienteColKey, row.getObjectKey(), ((RealmObjectProxy) value).realmGet$proxyState().getRow$realm().getObjectKey(), true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().nullifyLink(columnInfo.clienteColKey);
            return;
        }
        proxyState.checkValidObject(value);
        proxyState.getRow$realm().setLink(columnInfo.clienteColKey, ((RealmObjectProxy) value).realmGet$proxyState().getRow$realm().getObjectKey());
    }

    @Override
    public com.example.movieshow_store.models.Cd realmGet$cd() {
        proxyState.getRealm$realm().checkIfValid();
        if (proxyState.getRow$realm().isNullLink(columnInfo.cdColKey)) {
            return null;
        }
        return proxyState.getRealm$realm().get(com.example.movieshow_store.models.Cd.class, proxyState.getRow$realm().getLink(columnInfo.cdColKey), false, Collections.<String>emptyList());
    }

    @Override
    public void realmSet$cd(com.example.movieshow_store.models.Cd value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            if (proxyState.getExcludeFields$realm().contains("cd")) {
                return;
            }
            if (value != null && !RealmObject.isManaged(value)) {
                value = ((Realm) proxyState.getRealm$realm()).copyToRealm(value);
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                // Table#nullifyLink() does not support default value. Just using Row.
                row.nullifyLink(columnInfo.cdColKey);
                return;
            }
            proxyState.checkValidObject(value);
            row.getTable().setLink(columnInfo.cdColKey, row.getObjectKey(), ((RealmObjectProxy) value).realmGet$proxyState().getRow$realm().getObjectKey(), true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().nullifyLink(columnInfo.cdColKey);
            return;
        }
        proxyState.checkValidObject(value);
        proxyState.getRow$realm().setLink(columnInfo.cdColKey, ((RealmObjectProxy) value).realmGet$proxyState().getRow$realm().getObjectKey());
    }

    @Override
    @SuppressWarnings("cast")
    public Date realmGet$fecha_prestamo() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.util.Date) proxyState.getRow$realm().getDate(columnInfo.fecha_prestamoColKey);
    }

    @Override
    public void realmSet$fecha_prestamo(Date value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'fecha_prestamo' to null.");
            }
            row.getTable().setDate(columnInfo.fecha_prestamoColKey, row.getObjectKey(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            throw new IllegalArgumentException("Trying to set non-nullable field 'fecha_prestamo' to null.");
        }
        proxyState.getRow$realm().setDate(columnInfo.fecha_prestamoColKey, value);
    }

    @Override
    @SuppressWarnings("cast")
    public float realmGet$precio() {
        proxyState.getRealm$realm().checkIfValid();
        return (float) proxyState.getRow$realm().getFloat(columnInfo.precioColKey);
    }

    @Override
    public void realmSet$precio(float value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setFloat(columnInfo.precioColKey, row.getObjectKey(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setFloat(columnInfo.precioColKey, value);
    }

    @Override
    @SuppressWarnings("cast")
    public int realmGet$dias() {
        proxyState.getRealm$realm().checkIfValid();
        return (int) proxyState.getRow$realm().getLong(columnInfo.diasColKey);
    }

    @Override
    public void realmSet$dias(int value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setLong(columnInfo.diasColKey, row.getObjectKey(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setLong(columnInfo.diasColKey, value);
    }

    @Override
    @SuppressWarnings("cast")
    public Date realmGet$fecha_devolucion() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.util.Date) proxyState.getRow$realm().getDate(columnInfo.fecha_devolucionColKey);
    }

    @Override
    public void realmSet$fecha_devolucion(Date value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'fecha_devolucion' to null.");
            }
            row.getTable().setDate(columnInfo.fecha_devolucionColKey, row.getObjectKey(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            throw new IllegalArgumentException("Trying to set non-nullable field 'fecha_devolucion' to null.");
        }
        proxyState.getRow$realm().setDate(columnInfo.fecha_devolucionColKey, value);
    }

    private static OsObjectSchemaInfo createExpectedObjectSchemaInfo() {
        OsObjectSchemaInfo.Builder builder = new OsObjectSchemaInfo.Builder("Alquiler", 7, 0);
        builder.addPersistedProperty("id", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedLinkProperty("cliente", RealmFieldType.OBJECT, "Cliente");
        builder.addPersistedLinkProperty("cd", RealmFieldType.OBJECT, "Cd");
        builder.addPersistedProperty("fecha_prestamo", RealmFieldType.DATE, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("precio", RealmFieldType.FLOAT, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("dias", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("fecha_devolucion", RealmFieldType.DATE, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        return builder.build();
    }

    public static OsObjectSchemaInfo getExpectedObjectSchemaInfo() {
        return expectedObjectSchemaInfo;
    }

    public static AlquilerColumnInfo createColumnInfo(OsSchemaInfo schemaInfo) {
        return new AlquilerColumnInfo(schemaInfo);
    }

    public static String getSimpleClassName() {
        return "Alquiler";
    }

    public static final class ClassNameHelper {
        public static final String INTERNAL_CLASS_NAME = "Alquiler";
    }

    @SuppressWarnings("cast")
    public static com.example.movieshow_store.models.Alquiler createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        final List<String> excludeFields = new ArrayList<String>(2);
        if (json.has("cliente")) {
            excludeFields.add("cliente");
        }
        if (json.has("cd")) {
            excludeFields.add("cd");
        }
        com.example.movieshow_store.models.Alquiler obj = realm.createObjectInternal(com.example.movieshow_store.models.Alquiler.class, true, excludeFields);

        final com_example_movieshow_store_models_AlquilerRealmProxyInterface objProxy = (com_example_movieshow_store_models_AlquilerRealmProxyInterface) obj;
        if (json.has("id")) {
            if (json.isNull("id")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'id' to null.");
            } else {
                objProxy.realmSet$id((int) json.getInt("id"));
            }
        }
        if (json.has("cliente")) {
            if (json.isNull("cliente")) {
                objProxy.realmSet$cliente(null);
            } else {
                com.example.movieshow_store.models.Cliente clienteObj = com_example_movieshow_store_models_ClienteRealmProxy.createOrUpdateUsingJsonObject(realm, json.getJSONObject("cliente"), update);
                objProxy.realmSet$cliente(clienteObj);
            }
        }
        if (json.has("cd")) {
            if (json.isNull("cd")) {
                objProxy.realmSet$cd(null);
            } else {
                com.example.movieshow_store.models.Cd cdObj = com_example_movieshow_store_models_CdRealmProxy.createOrUpdateUsingJsonObject(realm, json.getJSONObject("cd"), update);
                objProxy.realmSet$cd(cdObj);
            }
        }
        if (json.has("fecha_prestamo")) {
            if (json.isNull("fecha_prestamo")) {
                objProxy.realmSet$fecha_prestamo(null);
            } else {
                Object timestamp = json.get("fecha_prestamo");
                if (timestamp instanceof String) {
                    objProxy.realmSet$fecha_prestamo(JsonUtils.stringToDate((String) timestamp));
                } else {
                    objProxy.realmSet$fecha_prestamo(new Date(json.getLong("fecha_prestamo")));
                }
            }
        }
        if (json.has("precio")) {
            if (json.isNull("precio")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'precio' to null.");
            } else {
                objProxy.realmSet$precio((float) json.getDouble("precio"));
            }
        }
        if (json.has("dias")) {
            if (json.isNull("dias")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'dias' to null.");
            } else {
                objProxy.realmSet$dias((int) json.getInt("dias"));
            }
        }
        if (json.has("fecha_devolucion")) {
            if (json.isNull("fecha_devolucion")) {
                objProxy.realmSet$fecha_devolucion(null);
            } else {
                Object timestamp = json.get("fecha_devolucion");
                if (timestamp instanceof String) {
                    objProxy.realmSet$fecha_devolucion(JsonUtils.stringToDate((String) timestamp));
                } else {
                    objProxy.realmSet$fecha_devolucion(new Date(json.getLong("fecha_devolucion")));
                }
            }
        }
        return obj;
    }

    @SuppressWarnings("cast")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static com.example.movieshow_store.models.Alquiler createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        final com.example.movieshow_store.models.Alquiler obj = new com.example.movieshow_store.models.Alquiler();
        final com_example_movieshow_store_models_AlquilerRealmProxyInterface objProxy = (com_example_movieshow_store_models_AlquilerRealmProxyInterface) obj;
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (false) {
            } else if (name.equals("id")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$id((int) reader.nextInt());
                } else {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'id' to null.");
                }
            } else if (name.equals("cliente")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    objProxy.realmSet$cliente(null);
                } else {
                    com.example.movieshow_store.models.Cliente clienteObj = com_example_movieshow_store_models_ClienteRealmProxy.createUsingJsonStream(realm, reader);
                    objProxy.realmSet$cliente(clienteObj);
                }
            } else if (name.equals("cd")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    objProxy.realmSet$cd(null);
                } else {
                    com.example.movieshow_store.models.Cd cdObj = com_example_movieshow_store_models_CdRealmProxy.createUsingJsonStream(realm, reader);
                    objProxy.realmSet$cd(cdObj);
                }
            } else if (name.equals("fecha_prestamo")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    objProxy.realmSet$fecha_prestamo(null);
                } else if (reader.peek() == JsonToken.NUMBER) {
                    long timestamp = reader.nextLong();
                    if (timestamp > -1) {
                        objProxy.realmSet$fecha_prestamo(new Date(timestamp));
                    }
                } else {
                    objProxy.realmSet$fecha_prestamo(JsonUtils.stringToDate(reader.nextString()));
                }
            } else if (name.equals("precio")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$precio((float) reader.nextDouble());
                } else {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'precio' to null.");
                }
            } else if (name.equals("dias")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$dias((int) reader.nextInt());
                } else {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'dias' to null.");
                }
            } else if (name.equals("fecha_devolucion")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    objProxy.realmSet$fecha_devolucion(null);
                } else if (reader.peek() == JsonToken.NUMBER) {
                    long timestamp = reader.nextLong();
                    if (timestamp > -1) {
                        objProxy.realmSet$fecha_devolucion(new Date(timestamp));
                    }
                } else {
                    objProxy.realmSet$fecha_devolucion(JsonUtils.stringToDate(reader.nextString()));
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        return realm.copyToRealm(obj);
    }

    private static com_example_movieshow_store_models_AlquilerRealmProxy newProxyInstance(BaseRealm realm, Row row) {
        // Ignore default values to avoid creating unexpected objects from RealmModel/RealmList fields
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        objectContext.set(realm, row, realm.getSchema().getColumnInfo(com.example.movieshow_store.models.Alquiler.class), false, Collections.<String>emptyList());
        io.realm.com_example_movieshow_store_models_AlquilerRealmProxy obj = new io.realm.com_example_movieshow_store_models_AlquilerRealmProxy();
        objectContext.clear();
        return obj;
    }

    public static com.example.movieshow_store.models.Alquiler copyOrUpdate(Realm realm, AlquilerColumnInfo columnInfo, com.example.movieshow_store.models.Alquiler object, boolean update, Map<RealmModel,RealmObjectProxy> cache, Set<ImportFlag> flags) {
        if (object instanceof RealmObjectProxy && !RealmObject.isFrozen(object) && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null) {
            final BaseRealm otherRealm = ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm();
            if (otherRealm.threadId != realm.threadId) {
                throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
            }
            if (otherRealm.getPath().equals(realm.getPath())) {
                return object;
            }
        }
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        RealmObjectProxy cachedRealmObject = cache.get(object);
        if (cachedRealmObject != null) {
            return (com.example.movieshow_store.models.Alquiler) cachedRealmObject;
        }

        return copy(realm, columnInfo, object, update, cache, flags);
    }

    public static com.example.movieshow_store.models.Alquiler copy(Realm realm, AlquilerColumnInfo columnInfo, com.example.movieshow_store.models.Alquiler newObject, boolean update, Map<RealmModel,RealmObjectProxy> cache, Set<ImportFlag> flags) {
        RealmObjectProxy cachedRealmObject = cache.get(newObject);
        if (cachedRealmObject != null) {
            return (com.example.movieshow_store.models.Alquiler) cachedRealmObject;
        }

        com_example_movieshow_store_models_AlquilerRealmProxyInterface realmObjectSource = (com_example_movieshow_store_models_AlquilerRealmProxyInterface) newObject;

        Table table = realm.getTable(com.example.movieshow_store.models.Alquiler.class);
        OsObjectBuilder builder = new OsObjectBuilder(table, flags);

        // Add all non-"object reference" fields
        builder.addInteger(columnInfo.idColKey, realmObjectSource.realmGet$id());
        builder.addDate(columnInfo.fecha_prestamoColKey, realmObjectSource.realmGet$fecha_prestamo());
        builder.addFloat(columnInfo.precioColKey, realmObjectSource.realmGet$precio());
        builder.addInteger(columnInfo.diasColKey, realmObjectSource.realmGet$dias());
        builder.addDate(columnInfo.fecha_devolucionColKey, realmObjectSource.realmGet$fecha_devolucion());

        // Create the underlying object and cache it before setting any object/objectlist references
        // This will allow us to break any circular dependencies by using the object cache.
        Row row = builder.createNewObject();
        io.realm.com_example_movieshow_store_models_AlquilerRealmProxy realmObjectCopy = newProxyInstance(realm, row);
        cache.put(newObject, realmObjectCopy);

        // Finally add all fields that reference other Realm Objects, either directly or through a list
        com.example.movieshow_store.models.Cliente clienteObj = realmObjectSource.realmGet$cliente();
        if (clienteObj == null) {
            realmObjectCopy.realmSet$cliente(null);
        } else {
            com.example.movieshow_store.models.Cliente cachecliente = (com.example.movieshow_store.models.Cliente) cache.get(clienteObj);
            if (cachecliente != null) {
                realmObjectCopy.realmSet$cliente(cachecliente);
            } else {
                realmObjectCopy.realmSet$cliente(com_example_movieshow_store_models_ClienteRealmProxy.copyOrUpdate(realm, (com_example_movieshow_store_models_ClienteRealmProxy.ClienteColumnInfo) realm.getSchema().getColumnInfo(com.example.movieshow_store.models.Cliente.class), clienteObj, update, cache, flags));
            }
        }

        com.example.movieshow_store.models.Cd cdObj = realmObjectSource.realmGet$cd();
        if (cdObj == null) {
            realmObjectCopy.realmSet$cd(null);
        } else {
            com.example.movieshow_store.models.Cd cachecd = (com.example.movieshow_store.models.Cd) cache.get(cdObj);
            if (cachecd != null) {
                realmObjectCopy.realmSet$cd(cachecd);
            } else {
                realmObjectCopy.realmSet$cd(com_example_movieshow_store_models_CdRealmProxy.copyOrUpdate(realm, (com_example_movieshow_store_models_CdRealmProxy.CdColumnInfo) realm.getSchema().getColumnInfo(com.example.movieshow_store.models.Cd.class), cdObj, update, cache, flags));
            }
        }

        return realmObjectCopy;
    }

    public static long insert(Realm realm, com.example.movieshow_store.models.Alquiler object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && !RealmObject.isFrozen(object) && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getObjectKey();
        }
        Table table = realm.getTable(com.example.movieshow_store.models.Alquiler.class);
        long tableNativePtr = table.getNativePtr();
        AlquilerColumnInfo columnInfo = (AlquilerColumnInfo) realm.getSchema().getColumnInfo(com.example.movieshow_store.models.Alquiler.class);
        long colKey = OsObject.createRow(table);
        cache.put(object, colKey);
        Table.nativeSetLong(tableNativePtr, columnInfo.idColKey, colKey, ((com_example_movieshow_store_models_AlquilerRealmProxyInterface) object).realmGet$id(), false);

        com.example.movieshow_store.models.Cliente clienteObj = ((com_example_movieshow_store_models_AlquilerRealmProxyInterface) object).realmGet$cliente();
        if (clienteObj != null) {
            Long cachecliente = cache.get(clienteObj);
            if (cachecliente == null) {
                cachecliente = com_example_movieshow_store_models_ClienteRealmProxy.insert(realm, clienteObj, cache);
            }
            Table.nativeSetLink(tableNativePtr, columnInfo.clienteColKey, colKey, cachecliente, false);
        }

        com.example.movieshow_store.models.Cd cdObj = ((com_example_movieshow_store_models_AlquilerRealmProxyInterface) object).realmGet$cd();
        if (cdObj != null) {
            Long cachecd = cache.get(cdObj);
            if (cachecd == null) {
                cachecd = com_example_movieshow_store_models_CdRealmProxy.insert(realm, cdObj, cache);
            }
            Table.nativeSetLink(tableNativePtr, columnInfo.cdColKey, colKey, cachecd, false);
        }
        java.util.Date realmGet$fecha_prestamo = ((com_example_movieshow_store_models_AlquilerRealmProxyInterface) object).realmGet$fecha_prestamo();
        if (realmGet$fecha_prestamo != null) {
            Table.nativeSetTimestamp(tableNativePtr, columnInfo.fecha_prestamoColKey, colKey, realmGet$fecha_prestamo.getTime(), false);
        }
        Table.nativeSetFloat(tableNativePtr, columnInfo.precioColKey, colKey, ((com_example_movieshow_store_models_AlquilerRealmProxyInterface) object).realmGet$precio(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.diasColKey, colKey, ((com_example_movieshow_store_models_AlquilerRealmProxyInterface) object).realmGet$dias(), false);
        java.util.Date realmGet$fecha_devolucion = ((com_example_movieshow_store_models_AlquilerRealmProxyInterface) object).realmGet$fecha_devolucion();
        if (realmGet$fecha_devolucion != null) {
            Table.nativeSetTimestamp(tableNativePtr, columnInfo.fecha_devolucionColKey, colKey, realmGet$fecha_devolucion.getTime(), false);
        }
        return colKey;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.example.movieshow_store.models.Alquiler.class);
        long tableNativePtr = table.getNativePtr();
        AlquilerColumnInfo columnInfo = (AlquilerColumnInfo) realm.getSchema().getColumnInfo(com.example.movieshow_store.models.Alquiler.class);
        com.example.movieshow_store.models.Alquiler object = null;
        while (objects.hasNext()) {
            object = (com.example.movieshow_store.models.Alquiler) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && !RealmObject.isFrozen(object) && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getObjectKey());
                continue;
            }
            long colKey = OsObject.createRow(table);
            cache.put(object, colKey);
            Table.nativeSetLong(tableNativePtr, columnInfo.idColKey, colKey, ((com_example_movieshow_store_models_AlquilerRealmProxyInterface) object).realmGet$id(), false);

            com.example.movieshow_store.models.Cliente clienteObj = ((com_example_movieshow_store_models_AlquilerRealmProxyInterface) object).realmGet$cliente();
            if (clienteObj != null) {
                Long cachecliente = cache.get(clienteObj);
                if (cachecliente == null) {
                    cachecliente = com_example_movieshow_store_models_ClienteRealmProxy.insert(realm, clienteObj, cache);
                }
                table.setLink(columnInfo.clienteColKey, colKey, cachecliente, false);
            }

            com.example.movieshow_store.models.Cd cdObj = ((com_example_movieshow_store_models_AlquilerRealmProxyInterface) object).realmGet$cd();
            if (cdObj != null) {
                Long cachecd = cache.get(cdObj);
                if (cachecd == null) {
                    cachecd = com_example_movieshow_store_models_CdRealmProxy.insert(realm, cdObj, cache);
                }
                table.setLink(columnInfo.cdColKey, colKey, cachecd, false);
            }
            java.util.Date realmGet$fecha_prestamo = ((com_example_movieshow_store_models_AlquilerRealmProxyInterface) object).realmGet$fecha_prestamo();
            if (realmGet$fecha_prestamo != null) {
                Table.nativeSetTimestamp(tableNativePtr, columnInfo.fecha_prestamoColKey, colKey, realmGet$fecha_prestamo.getTime(), false);
            }
            Table.nativeSetFloat(tableNativePtr, columnInfo.precioColKey, colKey, ((com_example_movieshow_store_models_AlquilerRealmProxyInterface) object).realmGet$precio(), false);
            Table.nativeSetLong(tableNativePtr, columnInfo.diasColKey, colKey, ((com_example_movieshow_store_models_AlquilerRealmProxyInterface) object).realmGet$dias(), false);
            java.util.Date realmGet$fecha_devolucion = ((com_example_movieshow_store_models_AlquilerRealmProxyInterface) object).realmGet$fecha_devolucion();
            if (realmGet$fecha_devolucion != null) {
                Table.nativeSetTimestamp(tableNativePtr, columnInfo.fecha_devolucionColKey, colKey, realmGet$fecha_devolucion.getTime(), false);
            }
        }
    }

    public static long insertOrUpdate(Realm realm, com.example.movieshow_store.models.Alquiler object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && !RealmObject.isFrozen(object) && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getObjectKey();
        }
        Table table = realm.getTable(com.example.movieshow_store.models.Alquiler.class);
        long tableNativePtr = table.getNativePtr();
        AlquilerColumnInfo columnInfo = (AlquilerColumnInfo) realm.getSchema().getColumnInfo(com.example.movieshow_store.models.Alquiler.class);
        long colKey = OsObject.createRow(table);
        cache.put(object, colKey);
        Table.nativeSetLong(tableNativePtr, columnInfo.idColKey, colKey, ((com_example_movieshow_store_models_AlquilerRealmProxyInterface) object).realmGet$id(), false);

        com.example.movieshow_store.models.Cliente clienteObj = ((com_example_movieshow_store_models_AlquilerRealmProxyInterface) object).realmGet$cliente();
        if (clienteObj != null) {
            Long cachecliente = cache.get(clienteObj);
            if (cachecliente == null) {
                cachecliente = com_example_movieshow_store_models_ClienteRealmProxy.insertOrUpdate(realm, clienteObj, cache);
            }
            Table.nativeSetLink(tableNativePtr, columnInfo.clienteColKey, colKey, cachecliente, false);
        } else {
            Table.nativeNullifyLink(tableNativePtr, columnInfo.clienteColKey, colKey);
        }

        com.example.movieshow_store.models.Cd cdObj = ((com_example_movieshow_store_models_AlquilerRealmProxyInterface) object).realmGet$cd();
        if (cdObj != null) {
            Long cachecd = cache.get(cdObj);
            if (cachecd == null) {
                cachecd = com_example_movieshow_store_models_CdRealmProxy.insertOrUpdate(realm, cdObj, cache);
            }
            Table.nativeSetLink(tableNativePtr, columnInfo.cdColKey, colKey, cachecd, false);
        } else {
            Table.nativeNullifyLink(tableNativePtr, columnInfo.cdColKey, colKey);
        }
        java.util.Date realmGet$fecha_prestamo = ((com_example_movieshow_store_models_AlquilerRealmProxyInterface) object).realmGet$fecha_prestamo();
        if (realmGet$fecha_prestamo != null) {
            Table.nativeSetTimestamp(tableNativePtr, columnInfo.fecha_prestamoColKey, colKey, realmGet$fecha_prestamo.getTime(), false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.fecha_prestamoColKey, colKey, false);
        }
        Table.nativeSetFloat(tableNativePtr, columnInfo.precioColKey, colKey, ((com_example_movieshow_store_models_AlquilerRealmProxyInterface) object).realmGet$precio(), false);
        Table.nativeSetLong(tableNativePtr, columnInfo.diasColKey, colKey, ((com_example_movieshow_store_models_AlquilerRealmProxyInterface) object).realmGet$dias(), false);
        java.util.Date realmGet$fecha_devolucion = ((com_example_movieshow_store_models_AlquilerRealmProxyInterface) object).realmGet$fecha_devolucion();
        if (realmGet$fecha_devolucion != null) {
            Table.nativeSetTimestamp(tableNativePtr, columnInfo.fecha_devolucionColKey, colKey, realmGet$fecha_devolucion.getTime(), false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.fecha_devolucionColKey, colKey, false);
        }
        return colKey;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.example.movieshow_store.models.Alquiler.class);
        long tableNativePtr = table.getNativePtr();
        AlquilerColumnInfo columnInfo = (AlquilerColumnInfo) realm.getSchema().getColumnInfo(com.example.movieshow_store.models.Alquiler.class);
        com.example.movieshow_store.models.Alquiler object = null;
        while (objects.hasNext()) {
            object = (com.example.movieshow_store.models.Alquiler) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && !RealmObject.isFrozen(object) && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getObjectKey());
                continue;
            }
            long colKey = OsObject.createRow(table);
            cache.put(object, colKey);
            Table.nativeSetLong(tableNativePtr, columnInfo.idColKey, colKey, ((com_example_movieshow_store_models_AlquilerRealmProxyInterface) object).realmGet$id(), false);

            com.example.movieshow_store.models.Cliente clienteObj = ((com_example_movieshow_store_models_AlquilerRealmProxyInterface) object).realmGet$cliente();
            if (clienteObj != null) {
                Long cachecliente = cache.get(clienteObj);
                if (cachecliente == null) {
                    cachecliente = com_example_movieshow_store_models_ClienteRealmProxy.insertOrUpdate(realm, clienteObj, cache);
                }
                Table.nativeSetLink(tableNativePtr, columnInfo.clienteColKey, colKey, cachecliente, false);
            } else {
                Table.nativeNullifyLink(tableNativePtr, columnInfo.clienteColKey, colKey);
            }

            com.example.movieshow_store.models.Cd cdObj = ((com_example_movieshow_store_models_AlquilerRealmProxyInterface) object).realmGet$cd();
            if (cdObj != null) {
                Long cachecd = cache.get(cdObj);
                if (cachecd == null) {
                    cachecd = com_example_movieshow_store_models_CdRealmProxy.insertOrUpdate(realm, cdObj, cache);
                }
                Table.nativeSetLink(tableNativePtr, columnInfo.cdColKey, colKey, cachecd, false);
            } else {
                Table.nativeNullifyLink(tableNativePtr, columnInfo.cdColKey, colKey);
            }
            java.util.Date realmGet$fecha_prestamo = ((com_example_movieshow_store_models_AlquilerRealmProxyInterface) object).realmGet$fecha_prestamo();
            if (realmGet$fecha_prestamo != null) {
                Table.nativeSetTimestamp(tableNativePtr, columnInfo.fecha_prestamoColKey, colKey, realmGet$fecha_prestamo.getTime(), false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.fecha_prestamoColKey, colKey, false);
            }
            Table.nativeSetFloat(tableNativePtr, columnInfo.precioColKey, colKey, ((com_example_movieshow_store_models_AlquilerRealmProxyInterface) object).realmGet$precio(), false);
            Table.nativeSetLong(tableNativePtr, columnInfo.diasColKey, colKey, ((com_example_movieshow_store_models_AlquilerRealmProxyInterface) object).realmGet$dias(), false);
            java.util.Date realmGet$fecha_devolucion = ((com_example_movieshow_store_models_AlquilerRealmProxyInterface) object).realmGet$fecha_devolucion();
            if (realmGet$fecha_devolucion != null) {
                Table.nativeSetTimestamp(tableNativePtr, columnInfo.fecha_devolucionColKey, colKey, realmGet$fecha_devolucion.getTime(), false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.fecha_devolucionColKey, colKey, false);
            }
        }
    }

    public static com.example.movieshow_store.models.Alquiler createDetachedCopy(com.example.movieshow_store.models.Alquiler realmObject, int currentDepth, int maxDepth, Map<RealmModel, CacheData<RealmModel>> cache) {
        if (currentDepth > maxDepth || realmObject == null) {
            return null;
        }
        CacheData<RealmModel> cachedObject = cache.get(realmObject);
        com.example.movieshow_store.models.Alquiler unmanagedObject;
        if (cachedObject == null) {
            unmanagedObject = new com.example.movieshow_store.models.Alquiler();
            cache.put(realmObject, new RealmObjectProxy.CacheData<RealmModel>(currentDepth, unmanagedObject));
        } else {
            // Reuse cached object or recreate it because it was encountered at a lower depth.
            if (currentDepth >= cachedObject.minDepth) {
                return (com.example.movieshow_store.models.Alquiler) cachedObject.object;
            }
            unmanagedObject = (com.example.movieshow_store.models.Alquiler) cachedObject.object;
            cachedObject.minDepth = currentDepth;
        }
        com_example_movieshow_store_models_AlquilerRealmProxyInterface unmanagedCopy = (com_example_movieshow_store_models_AlquilerRealmProxyInterface) unmanagedObject;
        com_example_movieshow_store_models_AlquilerRealmProxyInterface realmSource = (com_example_movieshow_store_models_AlquilerRealmProxyInterface) realmObject;
        unmanagedCopy.realmSet$id(realmSource.realmGet$id());

        // Deep copy of cliente
        unmanagedCopy.realmSet$cliente(com_example_movieshow_store_models_ClienteRealmProxy.createDetachedCopy(realmSource.realmGet$cliente(), currentDepth + 1, maxDepth, cache));

        // Deep copy of cd
        unmanagedCopy.realmSet$cd(com_example_movieshow_store_models_CdRealmProxy.createDetachedCopy(realmSource.realmGet$cd(), currentDepth + 1, maxDepth, cache));
        unmanagedCopy.realmSet$fecha_prestamo(realmSource.realmGet$fecha_prestamo());
        unmanagedCopy.realmSet$precio(realmSource.realmGet$precio());
        unmanagedCopy.realmSet$dias(realmSource.realmGet$dias());
        unmanagedCopy.realmSet$fecha_devolucion(realmSource.realmGet$fecha_devolucion());

        return unmanagedObject;
    }

    @Override
    @SuppressWarnings("ArrayToString")
    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("Alquiler = proxy[");
        stringBuilder.append("{id:");
        stringBuilder.append(realmGet$id());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{cliente:");
        stringBuilder.append(realmGet$cliente() != null ? "Cliente" : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{cd:");
        stringBuilder.append(realmGet$cd() != null ? "Cd" : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{fecha_prestamo:");
        stringBuilder.append(realmGet$fecha_prestamo());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{precio:");
        stringBuilder.append(realmGet$precio());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{dias:");
        stringBuilder.append(realmGet$dias());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{fecha_devolucion:");
        stringBuilder.append(realmGet$fecha_devolucion());
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public ProxyState<?> realmGet$proxyState() {
        return proxyState;
    }

    @Override
    public int hashCode() {
        String realmName = proxyState.getRealm$realm().getPath();
        String tableName = proxyState.getRow$realm().getTable().getName();
        long colKey = proxyState.getRow$realm().getObjectKey();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (colKey ^ (colKey >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        com_example_movieshow_store_models_AlquilerRealmProxy aAlquiler = (com_example_movieshow_store_models_AlquilerRealmProxy)o;

        BaseRealm realm = proxyState.getRealm$realm();
        BaseRealm otherRealm = aAlquiler.proxyState.getRealm$realm();
        String path = realm.getPath();
        String otherPath = otherRealm.getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;
        if (realm.isFrozen() != otherRealm.isFrozen()) return false;
        if (!realm.sharedRealm.getVersionID().equals(otherRealm.sharedRealm.getVersionID())) {
            return false;
        }

        String tableName = proxyState.getRow$realm().getTable().getName();
        String otherTableName = aAlquiler.proxyState.getRow$realm().getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (proxyState.getRow$realm().getObjectKey() != aAlquiler.proxyState.getRow$realm().getObjectKey()) return false;

        return true;
    }
}
